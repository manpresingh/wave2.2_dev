<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AV_Company_Personnel__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Company Personnel</label>
        <referenceTo>AV_Company_Personnel_Assignment__c</referenceTo>
        <relationshipLabel>Company Personnel Presents</relationshipLabel>
        <relationshipName>Personnel_Presents</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AV_Monitoring_Activity_Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Monitoring Activity</label>
        <referenceTo>AV_Monitoring_Activity__c</referenceTo>
        <relationshipLabel>Company Personnel Present During the Visit</relationshipLabel>
        <relationshipName>Personnel_Presents1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AV_Monitoring_Activity_Staff__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Monitoring Activity</label>
        <referenceTo>AV_Monitoring_Activity__c</referenceTo>
        <relationshipLabel>Study Site Staff Present During the Visit</relationshipLabel>
        <relationshipName>Personnel_Presents</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AV_Personnel_Name__c</fullName>
        <description>This field contains the name of a personnel</description>
        <externalId>false</externalId>
        <formula>AV_Company_Personnel__r.AV_PersonnelName__c</formula>
        <label>Personnel Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AV_Role__c</fullName>
        <description>This field contains the title of a personnel</description>
        <externalId>false</externalId>
        <formula>AV_Company_Personnel__r.AV_Role_Name__c</formula>
        <label>Role</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AV_Study_Site_Staff_Assignment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Study Site Staff Assignment</label>
        <referenceTo>AV_Study_Site_Staff_Assignment__c</referenceTo>
        <relationshipLabel>Personnel Presents</relationshipLabel>
        <relationshipName>Personnel_Presents</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Personnel Present</label>
    <nameField>
        <displayFormat>PP-{0000000}</displayFormat>
        <label>Personnel Present Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Personnel Presents</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>AV_Company_Personnel</fullName>
        <active>true</active>
        <label>Company Personnel</label>
    </recordTypes>
    <recordTypes>
        <fullName>AV_Site_Staff</fullName>
        <active>true</active>
        <label>Site Staff</label>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>AV_Assign_to_Visit</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Assign to Visit</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/apex/AV_StaffSearchAndAssignPage?monId={!AV_Monitoring_Activity__c.Id}&amp;siteId={!AV_Monitoring_Activity__c.AV_Study_SiteId__c}</url>
    </webLinks>
    <webLinks>
        <fullName>AV_Assign_to_Visit_CompanyPersonnel</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Assign to Visit</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/apex/AV_Assign_to_visit_monitoring?Monitoring={!AV_Monitoring_Activity__c.Id}</url>
    </webLinks>
</CustomObject>
