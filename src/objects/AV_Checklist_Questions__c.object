<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AV_Answer_Options__c</fullName>
        <description>This field defines which answer options will be displayed on the checklist edit page.</description>
        <externalId>false</externalId>
        <label>Answer Options</label>
        <picklist>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>N/A</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>AV_Applicable_Study__c</fullName>
        <description>This field contains the specific study(ies) that a checklist question can be applied to..</description>
        <externalId>false</externalId>
        <label>Applicable Study?</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AV_Checklist_Question_Status__c</fullName>
        <description>This field defines the status of a checklist question.</description>
        <externalId>false</externalId>
        <label>Checklist Question Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Draft</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AV_If_No_Prompt_to_log_observation__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field defines if the system will display the Observation Suggestion pop-up when the user edits the checklist question from a monitoring activity and selects &apos;No&apos; as the answer.</description>
        <externalId>false</externalId>
        <label>If ‘No’- Prompt to log observation?</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AV_Question__c</fullName>
        <description>This field contains the question of the checklist question.</description>
        <externalId>false</externalId>
        <label>Question</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AV_Reason_for_N_A_Picklist__c</fullName>
        <description>This field defines the picklist value of the &apos;Reason for N/A&apos; field on the checklist edit page.</description>
        <externalId>false</externalId>
        <label>‘Reason for N/A’ Picklist</label>
        <picklist>
            <picklistValues>
                <fullName>Conducted by blinded monitor only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Conducted by unblinded monitor only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IP previously returned / destroyed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Lack of enrollment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No IP in study</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No IP required for study</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not required due to focused monitoring visit</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not required per monitoring plan</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PI left institution</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Visit for Investigative Site file only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Visit for Source Document/facility visit only</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Visit to train study staff</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>AV_Reason_for_No_Picklist__c</fullName>
        <description>This field defines the picklist value of the &apos;Reason for No&apos; field on the checklist edit page.</description>
        <externalId>false</externalId>
        <label>Reason for No’ Picklist</label>
        <picklist>
            <picklistValues>
                <fullName>Lack of enrollment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PI left institution</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sponsor Directed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Site Never enrolled</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Study terminated</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Site closing–no PI transfer</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>AV_Show_Comments__c</fullName>
        <externalId>false</externalId>
        <label>Show Comments</label>
        <picklist>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>N/A</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>AV_Study_Specific_Checklist_Question__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field defined if a checklist question only applies to specific studies.</description>
        <externalId>false</externalId>
        <label>Study Specific Checklist Question?</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AV_Suggested_Observation_Category__c</fullName>
        <description>This field defines which category will be displayed on the Observation Suggestion pop-up when the user edits the checklist question from a monitoring activity and selects &apos;No&apos; as the answer .</description>
        <externalId>false</externalId>
        <label>Suggested Observation Category</label>
        <picklist>
            <picklistValues>
                <fullName>Facilities / Equipment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ICF/Subject Information</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Investigational Product (IP)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Investigative Site File</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Investigator Responsibilities</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Medical Communication</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other non-study specific observations</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Protocol Compliance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Risk Signal</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Site Training</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Source Document / Data</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Study System</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Subject Transfer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Supplies</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Trial/Product/Development Master File</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Vendor Performance</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AV_Topic__c</fullName>
        <description>This field contains the topic of the checklist question.</description>
        <externalId>false</externalId>
        <label>Topic</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Checklist Question</label>
    <listViews>
        <fullName>AV_All_checklist_questions</fullName>
        <columns>NAME</columns>
        <columns>AV_Question__c</columns>
        <columns>AV_Topic__c</columns>
        <columns>AV_Checklist_Question_Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All checklist questions</label>
    </listViews>
    <nameField>
        <displayFormat>CKQ-{0000000}</displayFormat>
        <label>Checklist Question Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Checklist Questions</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>AV_Topic__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AV_Question__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AV_Answer_Options__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AV_Study_Specific_Checklist_Question__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AV_Checklist_Question_Status__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>AV_Checklist_Question_Status__c</lookupFilterFields>
        <lookupFilterFields>AV_Topic__c</lookupFilterFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>AV_Checklist_Question_Status__c</searchFilterFields>
        <searchFilterFields>AV_Topic__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>AV_Applicable_Study_Required_Rule</fullName>
        <active>true</active>
        <description>Applicable Study?: Mandatory field if &apos;Study Specific Checklist Question?&apos; is checked</description>
        <errorConditionFormula>AND(AV_Study_Specific_Checklist_Question__c ,ISBLANK( AV_Applicable_Study__c ))</errorConditionFormula>
        <errorDisplayField>AV_Applicable_Study__c</errorDisplayField>
        <errorMessage>Missing Mandatory Fields</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>AV_New_Question_Draft_Status_Rule</fullName>
        <active>true</active>
        <description>New Checklist Question status should be in Draft Status</description>
        <errorConditionFormula>AND( ISNEW() , NOT( ISPICKVAL( AV_Checklist_Question_Status__c , &quot;Draft&quot;) ))</errorConditionFormula>
        <errorDisplayField>AV_Checklist_Question_Status__c</errorDisplayField>
        <errorMessage>New Checklist Question status should be in Draft Status</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>AV_Prevent_Specific_Status_Changes_Rule</fullName>
        <active>true</active>
        <description>Prevent save if the user changes the &apos;Checklist Question Status&apos; from :
Active to Draft
Inactive to Draft
Inactive to Active</description>
        <errorConditionFormula>OR(
 AND(
  ISPICKVAL(PRIORVALUE( AV_Checklist_Question_Status__c ),&quot;Active&quot;),       
  ISPICKVAL(AV_Checklist_Question_Status__c ,&quot;Draft&quot;)
 ),
 AND(
  ISPICKVAL(PRIORVALUE(AV_Checklist_Question_Status__c ),&quot;Inactive&quot;), 
  ISPICKVAL(AV_Checklist_Question_Status__c ,&quot;Draft&quot;)
 ),
AND(
  ISPICKVAL(PRIORVALUE(AV_Checklist_Question_Status__c ),&quot;Inactive&quot;), 
  ISPICKVAL(AV_Checklist_Question_Status__c ,&quot;Active&quot;)
 )
)</errorConditionFormula>
        <errorDisplayField>AV_Checklist_Question_Status__c</errorDisplayField>
        <errorMessage>Selected status change not allowed</errorMessage>
    </validationRules>
</CustomObject>
