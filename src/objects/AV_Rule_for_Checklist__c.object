<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AV_Checklist_Question__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field contains the name link to the checklist question that will be assigned to a rule configuration.</description>
        <externalId>false</externalId>
        <label>Checklist Question Name</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Checklist Question should be Active before associating it to that Rule.</errorMessage>
            <filterItems>
                <field>AV_Checklist_Questions__c.AV_Checklist_Question_Status__c</field>
                <operation>equals</operation>
                <value>Active</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>AV_Checklist_Questions__c</referenceTo>
        <relationshipLabel>Rule for Checklist</relationshipLabel>
        <relationshipName>Checklist_Response</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AV_Rule_Configuration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field contains the name link to the rule configuration that the checklist question will be assigned to .</description>
        <externalId>false</externalId>
        <label>Rule Configuration</label>
        <referenceTo>AV_Rule_Configuration__c</referenceTo>
        <relationshipName>Checklist_Response</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AV_Sequence__c</fullName>
        <externalId>false</externalId>
        <label>Sequence</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Rule for Checklist</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>CQA-{0000000}</displayFormat>
        <label>Rule for Checklist Name</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Rule for Checklist</pluralLabel>
    <searchLayouts>
        <searchFilterFields>AV_Checklist_Question__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>AV_RuleConfigurationStatus_Active_Inacti</fullName>
        <active>true</active>
        <description>Display validation message if the parent Rule Configuration record ‘Rule Configuration Status’ is “Active” or “Inactive. Error message “Checklist Assignment cannot be added or edited for Rule configuration with Active/Inactive Status.</description>
        <errorConditionFormula>OR( ISPICKVAL( AV_Rule_Configuration__r.AV_Status__c , &apos;Active&apos;) , ISPICKVAL( AV_Rule_Configuration__r.AV_Status__c , &apos;Inactive&apos;))</errorConditionFormula>
        <errorDisplayField>AV_Checklist_Question__c</errorDisplayField>
        <errorMessage>Checklist Assignment cannot be added or edited for Rule configuration with Active/Inactive Status.</errorMessage>
    </validationRules>
</CustomObject>
