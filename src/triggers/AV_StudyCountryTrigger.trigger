/* 
 * Class Name   : AV_StudyCountryTrigger 
 * Description  : Trigger Used to create related Event Dates
 * Created By   : Deloitte
 * Created On   : 03/11/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               03/11/2016                                    Initial version
 */
 
trigger AV_StudyCountryTrigger on AV_Study_Country__c(after update, after insert) {
    Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Study_Country__c');
     
     //Recursive Trigger execution Check 
     if(!AV_StaticConstants.TRIGGER_IS_RECURSIVE){
        
        // Starting After Operations        
        if(trigger.isafter) {
            // Starting Update Operations
            if(trigger.isupdate){
                //trigger to update the Study Site Name field whenever the country is changed in its parent Study Country      
                AV_StudyCountryTriggerHelper.updateStudySiteName(trigger.newmap , trigger.oldmap);  
            }
        }
    } 
      if(!profileByPass){   
     if((trigger.isAfter && trigger.isInsert) || (trigger.isAfter && trigger.isupdate)){
        AV_StudyCountryTriggerHelper.updateSubjectFirstDose(trigger.new);
    }
    }
}