/* 
 * Class Name   : AV_MonitoringActivityTrigger
 * Description  : Trigger Used to create checklist responses for Monitoring Avtivity
 * Created By   : Deloitte
 * Created On   : 11/09/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               11/09/2016                                    Initial version
 */

trigger AV_MonitoringActivityTrigger on AV_Monitoring_Activity__c (after insert, before insert) {
    Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Monitoring_Activity__c');//This Logic used to Bypass the Profiles 
    //Recursive Trigger execution Check 
     if(!AV_StaticConstants.TRIGGER_IS_RECURSIVE){
        // Starting After Operations        
        if(trigger.isAfter) {
            // Starting insert Operations
            if(trigger.isInsert){
                // Check whether this profiles can be bypassed from Custom settings
                if(!profileByPass){
                    //trigger to update the Study Site Name field whenever the country is changed in its parent Study Country      
                    AV_MonitoringActivityTriggerHelper.createChecklistResponses(trigger.new);  
                }
            }
        }
        if(trigger.isBefore){
            if(trigger.isInsert){
                if(!profileByPass){
                    AV_MonitoringActivityTriggerHelper.updateMonitoringOwner(trigger.new);  
                }
            }
        }
    }  
}