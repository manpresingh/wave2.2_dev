/* 
 * Class Name   : AV_StudySiteTrigger 
 * Description  : Trigger Used to create related Event Dates
 * Created By   : Deloitte
 * Created On   : 03/11/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                03/11/2016                                    Initial version
 */
 trigger AV_StudySiteTrigger on AV_Study_Site__c(after insert, after update){

    Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Study_Site__c');
    
    if(!AV_StaticConstants.TRIGGER_IS_RECURSIVE){
        // STARTING AFTER OPERATIONS
        if(trigger.isAfter){   

            // STARTING UPDATE OPERATIONS
            if(trigger.isupdate){
                
                // CHECK FOR PROFILE BYPASS
                if(!profileByPass){

                    //EXECUTION OF CANCELLING RELATED OBSERVATION IF STUDY SITE IS BEEN CANCALLED.
                     AV_StudySiteTriggerHelper.updateObservationAIonCancelledStudySite(trigger.new, trigger.oldMap);
                    // EXECUTION OF SEND SITE NOTIFICATION EMAIL
                    AV_StudySiteTriggerHelper.sendSiteSelectionEmail(trigger.newMap, trigger.oldMap);

                    // EXECUTION OF LOCKING / UNLOCKING OF STUDY SITE ON STATUS CHANGEVTO ENTRY ERROR
                    AV_StudySiteTriggerHelper.lockUnlockStudySiteEntryError(trigger.newMap, trigger.oldMap);                    
                }
            }

            // STARTING Insert OPERATIONS
            if(trigger.isInsert){
                // CHECK FOR PROFILE BYPASS
                if(!profileByPass){
                    // EXECUTION OF IRB/IEC APPROVAL RECORD CREATION FOR NEWLY CREATED STUDY SITES
                    AV_StudySiteTriggerHelper.createProtocolAmdmntApprovals(trigger.new);
                }
            }            
        }
    }
    
     if(!profileByPass){   
    if((trigger.isAfter && trigger.isInsert) || (trigger.isAfter && trigger.isupdate)){
        AV_StudySiteTriggerHelper.updateSubjectFirstDose(trigger.new);
    }
    }
    
}