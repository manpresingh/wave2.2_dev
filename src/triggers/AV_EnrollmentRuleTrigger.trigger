trigger AV_EnrollmentRuleTrigger on AV_Rule_for_Enrollment_Assignment__c (before insert) {
    if(Trigger.IsInsert&&Trigger.IsBefore){
        AV_EnrollmentRuleTriggerHelper.preventDuplicate(Trigger.New);
    }
}