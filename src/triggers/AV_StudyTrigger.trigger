/* 
 * Trigger Name : AV_StudyTrigger
 * Description  : Trigger Used to create related Event Dates
 * Created By   : Deloitte
 * Created On   : 03/11/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               03/11/2016                                    Initial version
 */
trigger AV_StudyTrigger on AV_Study__c (after update, before update, before insert) {
    Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Study_Country__c');
    //Recursive Trigger execution Check 
    if(!AV_StaticConstants.TRIGGER_IS_RECURSIVE){
        
        // Starting After Operations
        if(trigger.isafter) {

            // Starting Update Operations
            if(trigger.isupdate){

                //Logic to update the Name field on related Study Country and 
                //Study Site name when the protocal number is updated on Study record.
                AV_StudyTriggerHelper.updateCountrySiteName(trigger.oldMap, trigger.newMap);
                //Copy fieds to Study Release
                AV_StudyTriggerHelper.copyFieldsToStudyRelease(trigger.new,trigger.newMap,trigger.oldMap);
            }
        }
        
        if(trigger.isBefore && trigger.isupdate){
        }
    }
}