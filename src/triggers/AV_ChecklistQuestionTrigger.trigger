/********************************************************************************************************
* @author         Deloitte
* @description    This is a trigger on AV_Checklist_Questions__c object
* @date           11/9/2016
* 
* Modification Log:
* -------------------------------------------------------------------------------------------------------
* Developer                Date                   Modification ID      Description
* -------------------------------------------------------------------------------------------------------
* Deloitte                 11/9/2016                                   Initial version
*********************************************************************************************************/
trigger AV_ChecklistQuestionTrigger on AV_Checklist_Questions__c (Before Update) {
	Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), AV_StaticConstants.CHECKLIST_QUESTION_OBJ_NAME);
    AV_ChecklistQuestionTriggerHelper handler = new AV_ChecklistQuestionTriggerHelper();
    if(Trigger.isUpdate && Trigger.isBefore){ // before update
        if(!profileByPass) {
        	handler.OnBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        }
    }
    
}