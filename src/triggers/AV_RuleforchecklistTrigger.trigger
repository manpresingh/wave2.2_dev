/********************************************************************************************************
* @author         Deloitte
* @description    AV_RuleforchecklistTrigger 
*                
* @date           11/01/2016
* 
* Modification Log:
* -------------------------------------------------------------------------------------------------------
* Developer                Date                   Modification ID      Description
* -------------------------------------------------------------------------------------------------------
* Chandrakanth          11/01/2016                                   Initial version
*********************************************************************************************************/


trigger AV_RuleforchecklistTrigger on AV_Rule_for_Checklist__c  (before insert , before update ) {
    Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Rule_for_Checklist__c');

    if(!profileByPass){
         if((Trigger.isbefore && Trigger.isinsert)  || (Trigger.isbefore && Trigger.isupdate)){
        AV_RuleforchecklistTriggerHelper.performSequenceValidation(Trigger.new);
       }
         
        
    }

}