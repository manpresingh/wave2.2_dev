/* 
 * Class Name   : AV_Study_Releases_Trigger
 * Description  : 
 * Created By   : Deloitte
 * Created On   : 11/07/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               11/07/2016                                    Initial version
 */


trigger AV_Study_Releases_Trigger on AV_Study_Releases__c (before update, after insert,before insert) {
    Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Study_Releases__c');
    if(!AV_StaticConstants.TRIGGER_IS_RECURSIVE){
        if(Trigger.isUpdate && Trigger.isBefore){
            AV_StudyReleaseHelperClass.OnBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
        if(Trigger.isInsert && Trigger.isBefore){
            AV_StudyReleaseHelperClass.OnBeforeInsert(Trigger.new, Trigger.newMap);
        }
        if(Trigger.isInsert && Trigger.isAfter){
            AV_StudyReleaseHelperClass.OnAfterInsert(Trigger.new, Trigger.newMap);
        }
    }
    
}