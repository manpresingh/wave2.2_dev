/* 
 * Trigger Name : AV_Rule_for_Event_AssignmentTrigger
 * Description  : Trigger for AV_Rule_for_Event_Assignment__c
 * Created By   : Deloitte
 * Created On   : 07/11/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               07/11/2016                                    Initial version
 */
trigger AV_Rule_for_Event_AssignmentTrigger on AV_Rule_for_Event_Assignment__c (After update, After insert) {
Boolean profileByPass = AV_Common_Utilities.canProfileBeBypassed(userinfo.getProfileId(), 'AV_Rule_Configuration__c');
 if(!profileByPass){
if((trigger.isAfter && trigger.isupdate) || (trigger.isAfter && trigger.isinsert)){
       AV_Rule_Event_AssignmentTriggerHelper.CheckMultileStyudySetEvent(trigger.new);
    }
   } 
}