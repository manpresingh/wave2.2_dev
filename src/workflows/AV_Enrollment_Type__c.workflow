<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Enrollment_Type</fullName>
        <field>AV_Enrollment_Type__c</field>
        <formula>Name</formula>
        <name>Update Enrollment Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AV_Unique_Enrollment_Type</fullName>
        <actions>
            <name>Update_Enrollment_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT( ISNULL(  Name  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
