<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Monitoring_Activity_Name</fullName>
        <field>AV_Monitoring_Activity_Name__c</field>
        <formula>AV_Study_Site__r.AV_Study_Number__c +&apos;-&apos;+  TEXT(AV_Monitoring_Activity_Type__c) +&apos;-&apos;+ AV_Study_Site__r.AV_Principal_Investigator_RelatedList__c +&apos;-&apos;+ Name</formula>
        <name>Update Monitoring Activity Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Activity Name</fullName>
        <actions>
            <name>Update_Monitoring_Activity_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AV_Monitoring_Activity__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
