/* 
 * Class Name   : AV_MonitoringActivityTriggerHelper
 * Description  : AV_MonitoringActivityTriggerHelper Used for AV_MonitoringActivityTrigger
 * Created By   : Deloitte
 * Created On   : 11/09/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                11/09/2016                                    Initial version
 */
 
public class AV_MonitoringActivityTriggerHelper {

    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-09
   * @description      Used to create Checklist responses based on the rule configuration for that monitoring type
   *                   and checklist questions linked via rule for checklist object records; on creation of monitoring activity record
   * @Paramters        list<AV_Monitoring_Activity__c> newRecords --> list passed by trigger.new
   * @return           void
   *********************************************************************************************************/
    public static void createChecklistResponses(list<AV_Monitoring_Activity__c> newRecords){
        try{
            //set to store all the unique Monitoring activity types in case of multiple records being added
            set<String> MonitoringTypeLst = new set<String>();
            //map to store list of results corresponsing to their respective monitoring activity types
            Map<String, List<AV_Rule_for_Checklist__c>> rulenameChklstMap = new Map<String, List<AV_Rule_for_Checklist__c>>();
            //list of checklist response records to be finally created
            list<AV_Checklist__c> chklstResponseLst = new list<AV_Checklist__c>();
            //creating instance of checklist question so as to set appropriate fields
            AV_Checklist__c chklstRecNew = new AV_Checklist__c();
            for(AV_Monitoring_Activity__c rec: newRecords){
                MonitoringTypeLst.add(rec.AV_Monitoring_Activity_Type__c);
            }
            //query to get all the checklist questions related to the monitoring activity type via rule for checklist junction object
            list<AV_Rule_for_Checklist__c> chklstRuleLst = AV_SOQLQueryUtility_RuleForChecklist.getRuleForChecklistRecords(MonitoringTypeLst);
            for(AV_Rule_for_Checklist__c chklstRec: chklstRuleLst){
                if(!rulenameChklstMap.containsKey(chklstRec.AV_Rule_Configuration__r.AV_Monitoring_Activity_Type__c))
                    rulenameChklstMap.put(chklstRec.AV_Rule_Configuration__r.AV_Monitoring_Activity_Type__c, new list<AV_Rule_for_Checklist__c >());
                rulenameChklstMap.get(chklstRec.AV_Rule_Configuration__r.AV_Monitoring_Activity_Type__c).add(chklstRec);            
            }
            //iterating over multiple records being inserted
            for(AV_Monitoring_Activity__c monRec: newRecords){
                //check if the map contains the type
                if(rulenameChklstMap.containsKey(monRec.AV_Monitoring_Activity_Type__c)){
                    //fetching list of records against that type
                    List<AV_Rule_for_Checklist__c> rulechklstLst = rulenameChklstMap.get(monRec.AV_Monitoring_Activity_Type__c);
                    //iterating over all the records fetched using rule for checklist junction object
                    for(AV_Rule_for_Checklist__c rulechklstRec: rulechklstLst){
                        chklstRecNew = new AV_Checklist__c();
                        chklstRecNew.AV_Topic__c = rulechklstRec.AV_Checklist_Question__r.AV_Topic__c;
                        chklstRecNew.AV_Question__c = rulechklstRec.AV_Checklist_Question__r.AV_Question__c;
                        chklstRecNew.AV_Rule_for_Checklist__c = rulechklstRec.Id;
                        chklstRecNew.AV_Monitoring_Activity__c = monRec.Id;
                        //adding the new checklist question to the final list
                        chklstResponseLst.add(chklstRecNew);
                    }
                }
            }
            //checking if any question was found and added to the final list
            if(chklstResponseLst!=null && !chklstResponseLst.isEmpty()){
                //creation of records
                insert chklstResponseLst;
            }
        }
        catch(Exception commonException){
            AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_MonitoringActivityTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'createChecklistResponses', 'Error', TRUE);
        }
    }
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-17
   * @description      Stamps Monitoring Activity Owner with user contact and standard Owner with current loggedin User.
   * @Paramters        list<AV_Monitoring_Activity__c> newListMonAct--> list passed by trigger.new
   * @return           void
   *********************************************************************************************************/
    public static void updateMonitoringOwner(list<AV_Monitoring_Activity__c> newListMonAct){
    
        String loggedInUserId = String.valueOf(UserInfo.getUserID()).substring(0, 15);
       
        list<User> users = AV_SOQLQueryUtility_User.UserContactID(loggedInUserId);
        
        for(AV_Monitoring_Activity__c eachMonitoringActivity : newListMonAct){
            eachMonitoringActivity.AV_Monitoring_Activity_Owner__c = users[0].AV_ContactID__c;
            eachMonitoringActivity.ownerid=loggedInUserId;
        }
        
    }
    
}