/* 
 * Class Name   : AV_StudySiteTriggerHelper 
 * Description  : AV_StudySiteTriggerHelper Used for AV_StudySiteTriggers
 * Created By   : Deloitte
 * Created On   : 05/06/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte            05/06/2016                                    Initial version
 */
 
public class AV_StudySiteTriggerHelper {

    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-06-10
   * @description      Sends a notification email to Principal Investigator when an event is inserted with actual date or a Principal Investigator is changes on Study Site level.
   * @Paramters        Map<id, AV_Study_Site__c>: Map of new Study Sites; Map<id, AV_Study_Site__c>: Map of new Study Sites
   * @return           void
   *********************************************************************************************************/
    public static void sendSiteSelectionEmail(Map<id, AV_Study_Site__c> mapStudySiteNewMap, Map<id, AV_Study_Site__c> mapStudySiteOldMap){
        
        List<AV_Study_Site__c> listOfStudySite = new List<AV_Study_Site__c>();
        set<Id> setStudySiteIds = new set<Id>();

        for(AV_Study_Site__c eashStudySite : mapStudySiteNewMap.values()){
            if(eashStudySite.AV_Principal_Investigator__c != mapStudySiteOldMap.get(eashStudySite.id).AV_Principal_Investigator__c && eashStudySite.AV_Site_Selection_Notifications_sent__c && eashStudySite.AV_Site_Selection_Notification_Flag__c){
                setStudySiteIds.add(eashStudySite.id);
            }
        }
        if(setStudySiteIds != null && !setStudySiteIds.isEmpty()){
            listOfStudySite = [Select id, Name, AV_Country_Name__c, AV_site_selection_notification_timestamp__c, AV_Protocol_Number__c, AV_Study_Site_Reference__c, 
                                AV_Principal_Investigator_RelatedList__c, AV_Study_Country__r.AV_Study__r.AV_Sponsor_Name__c, AV_Study_Country__r.AV_Study__r.AV_Sponsor_Country__c, 
                                AV_Study_Country__r.AV_Study__r.AV_Sponsor_Address__c, AV_Principal_Investigator__c, AV_PI_Street_Address_ET__c, AV_Primary_Facility__c, 
                                AV_Study_Country__r.AV_Study__r.AV_Protocol_Title__c, AV_PI_Email_Address_ET__c, AV_Compound_Code_ET__c From AV_Study_Site__c Where Id IN: setStudySiteIds];
        }
        if(!listOfStudySite.isEmpty()){

            Messaging.SingleEmailMessage email;
            List<Messaging.Emailfileattachment> lstFileAttachments; //Sotres the list of attachments that needs to be sent.
            String templateBody = null; //Stores the main email attachment body.
            //List to store the Company Personnel related to the Study Site.
            List<AV_Company_Personnel_Assignment__c> lstOfCompanyPersonnel = new List<AV_Company_Personnel_Assignment__c>();
            //Map to store the Study Sites Id and its respective Primary Monitor information.
            Map<Id, AV_Company_Personnel_Assignment__c> mapStudySitePrimaryMonitor = new Map<Id, AV_Company_Personnel_Assignment__c>();
            //Map to store the Study Sites Id and its respective Document Management CSA (DAS).
            Map<Id, AV_Company_Personnel_Assignment__c> mapStudySiteDocumentManagement = new Map<Id, AV_Company_Personnel_Assignment__c>();
            //Map to store the Study Sites Id and its respective CSR-LU (DMS).
            Map<Id, AV_Company_Personnel_Assignment__c> mapStudySiteCSRLU = new Map<Id, AV_Company_Personnel_Assignment__c>();
            //Map to store the Study Sites Id and its respective Lab name for IIA template.
            Map<Id, String> mapStudySiteLabName = new Map<Id, String>();
            Boolean isFDCEUAtt = false; //Boolean to check if FDC EU attachment should be attach to the email.
            Boolean isFDCAtt = false; //Boolean to check if FDC attachment should be attach to the email
            Boolean isOUSIIAAtt = false; //Boolean to check if IIA attachment should be attach to the email
            List<String> lstOfCCAddress = new List<String>();// List to store all the CC Email addresss.
            List<Id> lstStudySiteIds = new List<Id>(); //Stores the list of Site id's to fetch company personnel.
            String labName = ''; //String to store lab name from Study record.
            Map<Id, List<Id>> mapObjIdAndTemplateId = new Map<id, List<Id>>();
            system.debug('@@#$$'+AV_EventDates_EmailTemplates_ET__c.getValues('FDC_EU').AV_Email_Template_ID__c);
            Id templateIdFCDEU = AV_EventDates_EmailTemplates_ET__c.getValues('FDC_EU').AV_Email_Template_ID__c;
            system.debug('@@#$$'+templateIdFCDEU);
            Id templateIdFCDAtt = AV_EventDates_EmailTemplates_ET__c.getValues('FCD').AV_Email_Template_ID__c;
            Id templateIdOUSIIA = AV_EventDates_EmailTemplates_ET__c.getValues('IIA').AV_Email_Template_ID__c;
            Id templateId = AV_EventDates_EmailTemplates_ET__c.getValues('Site Selection Notification').AV_Email_Template_ID__c;

            for(AV_Study_Site__c eachStudySiteRecord : listOfStudySite){
                lstStudySiteIds.add(eachStudySiteRecord.id);    
            }
            List<AV_Study_Site__c> lstStudtSiteLabName = new List<AV_Study_Site__c>();
            lstStudtSiteLabName = [Select id, AV_Study_Country__r.AV_Study__r.AV_Sponsor_Name__c, AV_Study_Country__r.AV_Study__r.AV_Sponsor_Country__c, AV_Study_Country__r.AV_Study__r.AV_Sponsor_Address__c From AV_Study_Site__c Where Id IN: lstStudySiteIds];
            if(lstStudtSiteLabName != null && !lstStudtSiteLabName.isEmpty()){
                for(AV_Study_Site__c eachStudySiteLabName : lstStudtSiteLabName){
                    labName = '';
                    if(eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Name__c != '' && eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Name__c != null){
                        labName = eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Name__c+',';
                    }
                    if(eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Address__c != '' && eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Address__c != null){
                        labName = labName + eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Address__c+',';
                    }
                    if(eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Country__c != '' && eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Country__c != null){
                        labName = labName + eachStudySiteLabName.AV_Study_Country__r.AV_Study__r.AV_Sponsor_Country__c;
                    }
                    mapStudySiteLabName.put(eachStudySiteLabName.id, labName);
                }
            }
            
            //Logic to create the map with study id and its respective Primary Monitor.
            if(!lstStudySiteIds.isEmpty()){
                Set<String> setRoleNames = new Set<String>{Label.AV_CRA, Label.AV_DAS, Label.AV_DMS};
                lstOfCompanyPersonnel = [Select id,AV_Personnel_Name__c, AV_Personnel_Name__r.Name, AV_Personnel_Email__c, AV_Personnel_Phone__c, 
                                        AV_Role_Name__c, AV_Start_Date__c, AV_End_Date__c, AV_Study_Site__c From AV_Company_Personnel_Assignment__c 
                                        Where AV_Study_Site__c IN: lstStudySiteIds AND AV_End_Date__c = null AND AV_Role_Name__c IN: setRoleNames];

                if(lstOfCompanyPersonnel != null && !lstOfCompanyPersonnel.isEmpty()){
                    for(AV_Company_Personnel_Assignment__c eachCompamyPersonnal : lstOfCompanyPersonnel){
                        if((mapStudySitePrimaryMonitor.containsKey(eachCompamyPersonnal.AV_Study_Site__c)) && 
                            (eachCompamyPersonnal.AV_Role_Name__c == Label.AV_CRA) && 
                            (mapStudySitePrimaryMonitor.get(eachCompamyPersonnal.AV_Study_Site__c).AV_Start_Date__c < eachCompamyPersonnal.AV_Start_Date__c)){
                            
                            mapStudySitePrimaryMonitor.put(eachCompamyPersonnal.AV_Study_Site__c, eachCompamyPersonnal);   
                                 
                        }else if(eachCompamyPersonnal.AV_Role_Name__c == Label.AV_CRA){
                          
                            mapStudySitePrimaryMonitor.put(eachCompamyPersonnal.AV_Study_Site__c, eachCompamyPersonnal);
                            
                        }else if((mapStudySiteDocumentManagement.containsKey(eachCompamyPersonnal.AV_Study_Site__c)) && 
                            (eachCompamyPersonnal.AV_Role_Name__c == Label.AV_DAS) && 
                            (mapStudySiteDocumentManagement.get(eachCompamyPersonnal.AV_Study_Site__c).AV_Start_Date__c < eachCompamyPersonnal.AV_Start_Date__c)){
                            
                            mapStudySiteDocumentManagement.put(eachCompamyPersonnal.AV_Study_Site__c, eachCompamyPersonnal);   
                                 
                        }else if(eachCompamyPersonnal.AV_Role_Name__c == label.AV_DAS){
                          
                            mapStudySiteDocumentManagement.put(eachCompamyPersonnal.AV_Study_Site__c, eachCompamyPersonnal);
                            
                        }else if((mapStudySiteCSRLU.containsKey(eachCompamyPersonnal.AV_Study_Site__c)) && 
                            (eachCompamyPersonnal.AV_Role_Name__c == Label.AV_DMS) && 
                            (mapStudySiteCSRLU.get(eachCompamyPersonnal.AV_Study_Site__c).AV_Start_Date__c < eachCompamyPersonnal.AV_Start_Date__c)){
                            
                            mapStudySiteCSRLU.put(eachCompamyPersonnal.AV_Study_Site__c, eachCompamyPersonnal);   
                                 
                        }else if(eachCompamyPersonnal.AV_Role_Name__c == Label.AV_DMS){
                          
                            mapStudySiteCSRLU.put(eachCompamyPersonnal.AV_Study_Site__c, eachCompamyPersonnal);
                            
                        }
                    } 
                } 
            }
            
            //Logic to Send the Email notification.
            List<Messaging.SingleEmailMessage> lstmails = new List<Messaging.SingleEmailMessage>();
            for(AV_Study_Site__c eachStudySite : listOfStudySite){

                if(AV_Common_Utilities.checkMandatoryFields(eachStudySite, mapStudySiteLabName) && mapStudySitePrimaryMonitor.containsKey(eachStudySite.id) && 
                    String.isNotBlank(mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Email__c) &&
                    String.isNotBlank(mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Name__r.Name)){
                    
                    isFDCEUAtt = false;
                    isFDCAtt = false;
                    isOUSIIAAtt = false;
                    
                    if(AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_Countries__c.contains(eachStudySite.AV_Country_Name__c)){

                        isFDCEUAtt = AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_FDC_EU_Attachment__c;
                        isFDCAtt = AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_FDC_Attachment__c;
                        isOUSIIAAtt = AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_IIA_Attachment__c;
                    }else if(AV_Site_Notification_Template_Selection__c.getValues('US').AV_Countries__c.contains(eachStudySite.AV_Country_Name__c)){

                        isFDCEUAtt = AV_Site_Notification_Template_Selection__c.getValues('US').AV_FDC_EU_Attachment__c;
                        isFDCAtt = AV_Site_Notification_Template_Selection__c.getValues('US').AV_FDC_Attachment__c;
                        isOUSIIAAtt = AV_Site_Notification_Template_Selection__c.getValues('US').AV_IIA_Attachment__c;
                    }else{
                        
                        isFDCEUAtt = AV_Site_Notification_Template_Selection__c.getValues('Others').AV_FDC_EU_Attachment__c;
                        isFDCAtt = AV_Site_Notification_Template_Selection__c.getValues('Others').AV_FDC_Attachment__c;
                        isOUSIIAAtt = AV_Site_Notification_Template_Selection__c.getValues('Others').AV_IIA_Attachment__c;
                    }
                    
                    if(isFDCAtt){
                        if(mapObjIdAndTemplateId.containsKey(eachStudySite.id)){
                            mapObjIdAndTemplateId.get(eachStudySite.id).add(templateIdFCDAtt);
                        }else{
                            mapObjIdAndTemplateId.put(eachStudySite.id , new List<Id>{templateIdFCDAtt});
                        }     
                    }    
                    if(isFDCEUAtt){
                        if(mapObjIdAndTemplateId.containsKey(eachStudySite.id)){
                            mapObjIdAndTemplateId.get(eachStudySite.id).add(templateIdFCDEU);
                        }else{
                            mapObjIdAndTemplateId.put(eachStudySite.id , new List<Id>{templateIdFCDEU});
                        }
                    }
                    if(isOUSIIAAtt){
                        if(mapObjIdAndTemplateId.containsKey(eachStudySite.id)){
                            mapObjIdAndTemplateId.get(eachStudySite.id).add(templateIdOUSIIA);
                        }else{
                            mapObjIdAndTemplateId.put(eachStudySite.id , new List<Id>{templateIdOUSIIA});
                        }
                    }

                    if(mapObjIdAndTemplateId.containsKey(eachStudySite.id)){
                        mapObjIdAndTemplateId.get(eachStudySite.id).add(templateId);
                    }else{
                        mapObjIdAndTemplateId.put(eachStudySite.id , new List<Id>{templateId});
                    }  
                }
            }

            Map<Id, Map<id, String>> mapObjIdAndHTMLbody = new Map<Id, Map<id, String>>();
            if(mapObjIdAndTemplateId != null && !mapObjIdAndTemplateId.isEmpty()){
                mapObjIdAndHTMLbody = AV_Common_Utilities.createHTMLAttachment(mapObjIdAndTemplateId, true);
            } 

            for(AV_Study_Site__c eachStudySite : listOfStudySite){
                if(mapStudySitePrimaryMonitor.containsKey(eachStudySite.id) && mapObjIdAndHTMLbody.containsKey(eachStudySite.id)){
                    email = new Messaging.SingleEmailMessage();
                    lstOfCCAddress = new List<String>();
                    lstFileAttachments = new List<Messaging.Emailfileattachment>();
                    templateBody = '';

                    if(mapObjIdAndHTMLbody.get(eachStudySite.id).containsKey(templateIdFCDAtt)){
                        lstFileAttachments.add(AV_Common_Utilities.createSiteEmailAttachment(mapObjIdAndHTMLbody.get(eachStudySite.id).get(templateIdFCDAtt), eachStudySite, 'FDC', null));
                    }    
                    if(mapObjIdAndHTMLbody.get(eachStudySite.id).containsKey(templateIdFCDEU)){
                        lstFileAttachments.add(AV_Common_Utilities.createSiteEmailAttachment(mapObjIdAndHTMLbody.get(eachStudySite.id).get(templateIdFCDEU), eachStudySite, 'FDC_EU', null));
                    }
                    if(mapObjIdAndHTMLbody.get(eachStudySite.id).containsKey(templateIdOUSIIA)){
                        lstFileAttachments.add(AV_Common_Utilities.createSiteEmailAttachment(mapObjIdAndHTMLbody.get(eachStudySite.id).get(templateIdOUSIIA), eachStudySite, 'IIA', mapStudySiteLabName));
                    }

                    templateBody = mapObjIdAndHTMLbody.get(eachStudySite.id).get(templateId);
                    templateBody = templateBody.replace('monitor_name', mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Name__r.Name);
                    templateBody = templateBody.replace('monitor_e-mail_address', mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Email__c);
                    lstOfCCAddress.add(mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Email__c);
                    
                    if(mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Phone__c != null && mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Phone__c != ''){
                        templateBody = templateBody.replace('monitor_telephone', mapStudySitePrimaryMonitor.get(eachStudySite.id).AV_Personnel_Phone__c);
                    }else{
                        templateBody = templateBody.replace('monitor_telephone', '');
                    }
                    if(mapStudySiteDocumentManagement.containsKey(eachStudySite.id) && 
                        mapStudySiteDocumentManagement.get(eachStudySite.id).AV_Personnel_Email__c != null && 
                        mapStudySiteDocumentManagement.get(eachStudySite.id).AV_Personnel_Email__c != ''){

                        lstOfCCAddress.add(mapStudySiteDocumentManagement.get(eachStudySite.id).AV_Personnel_Email__c);
                    }
                    if(mapStudySiteCSRLU.containsKey(eachStudySite.id) && 
                        mapStudySiteCSRLU.get(eachStudySite.id).AV_Personnel_Email__c != null && 
                        mapStudySiteCSRLU.get(eachStudySite.id).AV_Personnel_Email__c != ''){
                        
                        lstOfCCAddress.add(mapStudySiteCSRLU.get(eachStudySite.id).AV_Personnel_Email__c);
                    }
                    String strSubject = Label.AV_Study_Site_Email_Notification_Subject_I+' '+eachStudySite.AV_Protocol_Number__c+Label.AV_Study_Site_Email_Notification_Subject_II;
                    email = AV_Common_Utilities.prepareEmailBody(eachStudySite.id,'', null, templateBody, eachStudySite.AV_Principal_Investigator__c, strSubject, true, lstOfCCAddress);
                    email.setFileAttachments(lstFileAttachments);
                    eachStudySite.AV_Site_Selection_Notification_Timestamp__c = System.now();
                    lstmails.add(email);
                }   
            } 
            if(!lstmails.isEmpty()){
                try{
                    Messaging.sendEmail(lstmails);
                    AV_StaticConstants.TRIGGER_IS_RECURSIVE = TRUE;
                    update listOfStudySite;
                    AV_StaticConstants.TRIGGER_IS_RECURSIVE = FALSE;
                }catch(Exception commonException){
                    AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_StudySiteTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'sendSiteSelectionEmail', 'Error', TRUE); 
                }
                
            }
        }
    }

    /********************************************************************************************************
    * @author           Deloitte
    * @date             2016-06-20
    * @description      Method used to lock and Unlock Study Site when status is updated with Entry error. 
    * @paramters        mapStudySiteNewMap(Map<id, AV_Study_Site__c>): List of updated Study Site records.
    * @Paramters        mapStudySiteOldMap(Map<id, AV_Study_Site__c>): List of Study Site records with 
    *                   old values.
    * @return           void
    *********************************************************************************************************/
    public static void lockUnlockStudySiteEntryError(Map<id, AV_Study_Site__c> mapStudySiteNewMap, 
                                                    Map<id, AV_Study_Site__c> mapStudySiteOldMap){
        
        List<Id> studySiteIDLockList = new List<Id>();
        List<Id> studySiteIDUnLockList = new List<Id>();
        AV_Study_Site__c oldStudySiteVar = null;
        try{

            // ITERATE THROUGH UPDATED RECORDS AND FILTER THE RECORDS WHICH HAVE BEEN UPDATED FROM
            // ENTRY ERROR STATUS TO ANY & ANY TO ENTRY ERROR STATUS
            for(AV_Study_Site__c eachStudySite : mapStudySiteNewMap.values()){
                
                oldStudySiteVar = mapStudySiteOldMap.get(eachStudySite.id);
                system.debug('@@@@@@ the old map value is '+oldStudySiteVar);
                if( oldStudySiteVar != null && eachStudySite.AV_Study_Site_Status__c != mapStudySiteOldMap.get(eachStudySite.id).AV_Study_Site_Status__c) { 
                    if(Label.AV_Site_Status_Entry_Error.equalsIgnoreCase(eachStudySite.AV_Study_Site_Status__c)){
                        studySiteIDLockList.add(eachStudySite.Id);
                    } else if(Label.AV_Site_Status_Entry_Error.equalsIgnoreCase(oldStudySiteVar.AV_Study_Site_Status__c)){
                        studySiteIDUnLockList.add(eachStudySite.id);
                    }
                }
                oldStudySiteVar = null;
            }
            if(!studySiteIDLockList.isEmpty()){
                AV_StaticConstants.TRIGGER_IS_RECURSIVE = TRUE;
               // USING APEX LOCKING TO LOCK THE STUDY SITE RECORDS ON STATUS CHANGE TO ENTRY ERROR
               List<Approval.LockResult> lockResultVar =  Approval.lock(studySiteIDLockList);
               AV_StaticConstants.TRIGGER_IS_RECURSIVE = FALSE;
            }
            if(!studySiteIDUnLockList.isEmpty()){
                
                List<Id> studySiteUpdateLockList = new List<Id>();

                // CHECK LOCK STATUS OF RECORDS WHICH HAVE BEEN FILTERED FOR UN-LOCKING
                for( Id eachStudySiteId : studySiteIDUnLockList){
                    if(Approval.isLocked(eachStudySiteId)){
                        studySiteUpdateLockList.add(eachStudySiteId);
                    }
                }
                if(!studySiteUpdateLockList.isEmpty()){
                    AV_StaticConstants.TRIGGER_IS_RECURSIVE = TRUE;
                    // USING APEX LOCKING TO UNCLOCK THE STUDY SITE RECORDS ON STATUS CHANGE FROM ENTRY ERROR
                    List<Approval.UnlockResult> unlockResultVar = Approval.unlock(studySiteUpdateLockList);
                    AV_StaticConstants.TRIGGER_IS_RECURSIVE = FALSE;                    
                }
            }
        } catch(Exception commonException){
             AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_StudySiteTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'lockUnlockStudySiteEntryError', 'Error', TRUE); 
        }     
    }

    /********************************************************************************************************
    * @author           Deloitte
    * @date             2016-06-20
    * @description      Method used to approval records for Study Site after insertion.
    *                   1) Checks if related Study Country has approval Records with level
    *                       as Country & Site or Site only.
    *                   2) If approval records exist with above criteria then create approval
    *                       records for newly created Study Site.
    * @paramters        studySiteList(List<AV_Study_Site__c>): List of newly created Study Site records.
    * @return           void
    *********************************************************************************************************/
    public static void createProtocolAmdmntApprovals(List<AV_Study_Site__c> studySiteList){
        
        Set<String> approvalLevelSet = new Set<String>();
        approvalLevelSet.add(Label.AV_Country_Site);
        approvalLevelSet.add(Label.AV_Site_Only);
        Map<Id, List<AV_Study_Site__c>> countrySiteMap = new Map<Id, List<AV_Study_Site__c>>();
        AV_Protocol_Approval_and_Amendment__c oIRBIECApproval = null;
        List<AV_Protocol_Approval_and_Amendment__c> siteApprovalInsertList = new List<AV_Protocol_Approval_and_Amendment__c>();
        
        try{
            // PREPARE MAP OF STUDY COUNTRY ID WITH RELATED STUDY SITES LIST
            for(AV_Study_Site__c eachStudySite :studySiteList ){
                if(countrySiteMap.containsKey(eachStudySite.AV_Study_Country__c)) {
                    countrySiteMap.get(eachStudySite.AV_Study_Country__c).add(eachStudySite);
                } else{
                    List<AV_Study_Site__c> tempSiteList = new List<AV_Study_Site__c>();
                    tempSiteList.add(eachStudySite);
                    countrySiteMap.put(eachStudySite.AV_Study_Country__c, tempSiteList);
                }
            }
            if(!countrySiteMap.isEmpty()){
                List<AV_Study_Site__c> relatedSiteList = null;

                // FETCH ALL APPORVAL RECORDS OF RELATED STUDY COUNTRIES WITH APPROVAL LEVEL AS COUNTRY & SITES
                // OR SITES ONLY
                for(AV_Protocol_Approval_and_Amendment__c eachApproval : [SELECT Id, AV_Protocol_Amendment__c, AV_Amendment_Number__c,  
                                                            RecordTypeId, AV_Study_Country_IRB_IEC__c 
                                                            FROM 
                                                            AV_Protocol_Approval_and_Amendment__c 
                                                            WHERE AV_Study_Country_IRB_IEC__c IN: countrySiteMap.keySet()  
                                                            AND AV_Is_Soft_Delete_Apex__c =: FALSE 
                                                            AND AV_Approval_Level__c IN: approvalLevelSet]){

                    relatedSiteList = countrySiteMap.get(eachApproval.AV_Study_Country_IRB_IEC__c);

                    // CREATE APPORVAL RECORDS FOR EACH NEWLY CREATED SITE IF STUDY COUNTRY HAS IRB/IEC APPROVALS WITH LEVEL 
                    // AS COUNTRY & SITES / SITES ONLY.
                    if(relatedSiteList != null && !relatedSiteList.isEmpty()){
                        for(AV_Study_Site__c relatedSite : relatedSiteList){
                            oIRBIECApproval = new AV_Protocol_Approval_and_Amendment__c();
                            oIRBIECApproval.RecordTypeId = eachApproval.RecordTypeId;
                            oIRBIECApproval.AV_Protocol_Amendment__c = eachApproval.AV_Protocol_Amendment__c;
                            oIRBIECApproval.AV_Study_Site_IRB_IEC__c = relatedSite.Id;
                            oIRBIECApproval.AV_External_Unique_Id__c =  AV_Common_Utilities.populateApprovalRecordExternalID(Label.AV_IRB_IEC_Record_Type, 
                                                                        String.valueOf(eachApproval.AV_Amendment_Number__c), relatedSite.AV_External_Unique_Id__c,
                                                                        null,FALSE);

                            siteApprovalInsertList.add(oIRBIECApproval);
                        }
                    }
                }
                // INSERT APPROVAL RECORDS FOR EACH NEWLY CREATED SITE 
                if(!siteApprovalInsertList.isEmpty()){
                    Database.SaveResult[] insertResult = Database.insert(siteApprovalInsertList, false);
                }
            }
        } catch(Exception commonException){
             AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_StudySiteTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'createProtocolAmdmntApprovals', 'Error', TRUE); 
        }     
    }

    /********************************************************************************************************
    * @author           Deloitte
    * @date             2016-08-29
    * @description      Method to cancel related Observations if study site is cancelled.
    * @paramters        lstTriggerNew(List<AV_Study_Site__c>): List of newly created Study Site records.
                        mapTriggerOld(Map<id,AV_Study_Site__c>): Trigger old map
    * @return           void
    *********************************************************************************************************/

    public static void updateObservationAIonCancelledStudySite(List<AV_Study_Site__c> lstTriggerNew, Map<id, AV_Study_Site__c> mapTriggerOld){
        Set<Id> studyIds = new Set<Id>(); 
        List<AV_Observation__c> observationsUpdate = new List<AV_Observation__c>();
        List<AV_Observation__c> observationRetrieve = new List<AV_Observation__c>();
        try{
            for(AV_Study_Site__c st: lstTriggerNew){
                //get records only if status is changed and changed to cancelled
                if(!String.isBlank(st.AV_Study_Site_Status__c) && mapTriggerOld.get(st.id).AV_Study_Site_Status__c != st.AV_Study_Site_Status__c && 
                    st.AV_Study_Site_Status__c.equals(label.AV_Study_Site_Study_Site_Status_Cancelled)){
                    studyIds.add(st.id);
                }
                
             }

             if(!studyIds.isEmpty() && studyIds.size()!=0){
                //query all observation related to study site. Observation should not be cancelled or completed status.
                /*observationRetrieve  =[SELECT id, name, AV_Status__c, AV_Observation_Closeout_Rationale__c,AV_Related_PI_Primary_Facility__r.AV_Study_Site_Status__c FROM AV_Observation__c WHERE AV_Related_PI_Primary_Facility__c IN :studyIds AND AV_Status__c <>: label.AV_Observation_status_value_Action_Closed
                                        AND AV_Status__c <>: label.AV_Observation_status_value_Cancelled LIMIT 50000];*/
                 observationRetrieve = AV_SOQLQueryUtility_Observation.getObservationRelatedToStudy(studyIds);
             }
            
            if(!observationRetrieve.isEmpty() && observationRetrieve.size()!=0){
                for(AV_Observation__c observation: observationRetrieve){
                    //Observation status should not be blank
                        observation.AV_Status__c = label.AV_Observation_status_value_Cancelled;
                        observation.AV_Observation_Closeout_Rationale__c = label.AV_Observation_field_Closeout_Rationale_value_On_Study_site_Cancel;
                        observationsUpdate.add(observation);  
                    
                }
            }
            //updating related Observation 
            if(!observationsUpdate.isEmpty() && observationsUpdate.size()!=0){
                Database.SaveResult[] sr = Database.update(observationsUpdate,true);
            }
        }catch(Exception errorMessage){

            //logging errors
            AV_LOG_LogMessageUtility.logMessage(errorMessage, 'AV_StudySiteTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'updateObservationAIonCancelledStudySite', 'Error', TRUE); 
        }
    }
   /********************************************************************************************************
    * @author           Deloitte
    * @date             2016-11-03
    * @description      Method to update the field Last Subject First Dose on parents.
    * @paramters        lstTriggerNew(List<AV_Study_Site__c>): List of newly created Study Site records.
                       
    * @return           void
    *********************************************************************************************************/   
    public static void updateSubjectFirstDose(List<AV_Study_Site__c> lstTriggerNew){
        Set<id> setOfStudyCountryIds = new Set<id>();
        //Map<id, List<AV_Study_Site__c>> mapOfStudyCountryToStudySite = new Map<id, List<AV_Study_Site__c>>();
        Set<id>  setOfIDStudyCountryIsChecked = new Set<id>();
        Set<id>  setOfIDStudyIsChecked = new Set<id>();
        //Set<AV_Study_Country__c>  setStudyCountryToUpdate = new Set<AV_Study_Country__c>();
        //Set<AV_Study__c>  setStudyToUpdate = new Set<AV_Study__c>();
        List<AV_Study_Country__c>  setStudyCountryToUpdate = new List<AV_Study_Country__c>();
        List<AV_Study__c>  setStudyToUpdate = new List<AV_Study__c>();
        try{
        for(AV_Study_Site__c eachStudySite : lstTriggerNew){
            setOfStudyCountryIds.add(eachStudySite.AV_Study_Country__c);
            //mapOfIDStudyCountryToIsChecked.put(eachStudySite.AV_Study_Country__c, false);
        }
        for(AV_Study_Country__c eachStudyCountry : [select id, AV_Study__c, AV_Last_Subject_First_Dose__c, (select id, AV_Last_Subject_First_Dose__c from Study_Sites__r) from AV_Study_Country__c where id IN: setOfStudyCountryIds] ){
            //mapOfStudyCountryToStudySite.put(eachStudyCountry.id, eachStudyCountry.Study_Sites__r);
            Boolean isAllChecked = true;
            for(AV_Study_Site__c eachStudySite : eachStudyCountry.Study_Sites__r){
                if(eachStudySite.AV_Last_Subject_First_Dose__c)
                    continue;
                else{
                    isAllChecked = false;
                    break;
                }
            }
            if(isAllChecked){
            eachStudyCountry.AV_Last_Subject_First_Dose__c = true;
            }else{
                eachStudyCountry.AV_Last_Subject_First_Dose__c = false;
            }
            setOfIDStudyCountryIsChecked.add(eachStudyCountry.AV_Study__c); 
            setStudyCountryToUpdate.add(eachStudyCountry);
        }
        
        if(!setStudyCountryToUpdate.isEmpty())
            update setStudyCountryToUpdate;
        
        for(AV_Study__c eachStudy : [select id, AV_Last_Subject_First_Dose__c, (select id, AV_Last_Subject_First_Dose__c from Study_Countries__r) from AV_Study__c where id IN: setOfIDStudyCountryIsChecked] ){
            //mapOfStudyCountryToStudySite.put(eachStudyCountry.id, eachStudyCountry.Study_Sites__r);
            Boolean isAllChecked = true;
            for(AV_Study_Country__c eachStudyCountry : eachStudy.Study_Countries__r){
                if(eachStudyCountry.AV_Last_Subject_First_Dose__c)
                    continue;
                else{
                    isAllChecked = false;
                    break;
                }
            }
            if(isAllChecked){
            setOfIDStudyIsChecked.add(eachStudy.id);
            eachStudy.AV_Last_Subject_First_Dose__c = true;
            }else{
            eachStudy.AV_Last_Subject_First_Dose__c = false;    
            }
            setStudyToUpdate.add(eachStudy);            
        }
        
        if(!setStudyToUpdate.isEmpty())
            update setStudyToUpdate;
    }catch(Exception errorMessage){

            //logging errors
            AV_LOG_LogMessageUtility.logMessage(errorMessage, 'AV_StudySiteTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'updateSubjectFirstDose', 'Error', TRUE); 
        }
    }   
}