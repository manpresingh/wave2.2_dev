/*********************************************************************************************************
 * Class Name   : AV_SOQLQueryUtility_Study 
 * Description  : Generic service class for SOQL queries on AV_Study__share.
 * Created By   : Deloitte
 * Created On   : 11/9/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte        11/9/2016                                   Initial version
*/
    public with sharing class AV_SOQLQueryUtility_Rule_For_Enrollment{
        public AV_SOQLQueryUtility_Rule_For_Enrollment(){
 
    }
    
    public static map<ID,AV_Rule_for_Enrollment_Assignment__c > getRuleForEnrollment(){
        String query='select id, AV_Enrollment_Type__c from AV_Rule_for_Enrollment_Assignment__c';
        List<AV_Rule_for_Enrollment_Assignment__c> EnrollmentRuleInfo;
        EnrollmentRuleInfo=Database.query(query);
        map<ID,AV_Rule_for_Enrollment_Assignment__c > mapAREA = new map<ID,AV_Rule_for_Enrollment_Assignment__c >();
        for(AV_Rule_for_Enrollment_Assignment__c  objArea: EnrollmentRuleInfo){
              mapAREA.put(objArea.AV_Enrollment_Type__c,objArea);
    }
    
        return mapAREA;
    }
}