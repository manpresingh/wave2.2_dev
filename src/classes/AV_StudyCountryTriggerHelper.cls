/* 
 * Class Name   : AV_StudyCountryTriggerHelper 
 * Description  : AV_StudyCountryTriggerHelper Used for AV_StudyCountry Triggers
 * Created By   : Deloitte
 * Created On   : 04/12/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte            04/12/2016                                    Initial version
 */
 
public class AV_StudyCountryTriggerHelper {

    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-06-10
   * @description      This method is used to update the Study Site Name field whenever the country is changed in its parent Study Country.
   * @Paramters        Map<id, AV_Study_Country__c>: Map of new Study COuntries; Map<id, AV_Study_Country__c>: Map of Old STudy COuntries
   * @return           void
   *********************************************************************************************************/
    public static void updateStudySiteName(Map<id, AV_Study_Country__c> mapNewStudyCountryRecords, Map<id, AV_Study_Country__c> mapOldStudyCountryRecords){
        List<id> lstStudyCountryIds = new List<Id>();
        List<AV_Study_Site__c> lstStudySiteRecords = new List<AV_Study_Site__c>();
        
        for(id eachStudyCountryRec : mapNewStudyCountryRecords.keySet()){
            if(mapNewStudyCountryRecords.get(eachStudyCountryRec).AV_Country_Name__c != mapOldStudyCountryRecords.get(eachStudyCountryRec).AV_Country_Name__c){
                lstStudyCountryIds.add(eachStudyCountryRec);
            }
        }
        
        if(!lstStudyCountryIds.isEmpty()){
            lstStudySiteRecords = [Select id, Name, AV_Protocol_Number__c, AV_Country_Name__c, AV_Principal_Investigator_RelatedList__c, AV_Primary_Facility__r.Name, AV_Study_Country__c From AV_Study_Site__c Where AV_Study_Country__c IN: lstStudyCountryIds];    
            if(lstStudySiteRecords != null && !lstStudySiteRecords.isEmpty()){
                for(AV_Study_Site__c eachStudySite : lstStudySiteRecords){
                    eachStudySite.Name = eachStudySite.AV_Protocol_Number__c +' - '+eachStudySite.AV_Country_Name__c+' - '+eachStudySite.AV_Principal_Investigator_RelatedList__c+' - '+eachStudySite.AV_Primary_Facility__r.Name;
                }
                try{
                    update lstStudySiteRecords;
                }catch(Exception commonException){
                    AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_StudyCountryTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'createoriginalProtocol', 'Error', TRUE);
                }
            }
        }
    } 
    
    /********************************************************************************************************
    * @author           Deloitte
    * @date             2016-11-03
    * @description      Method to update the field Last Subject First Dose on parents.
    * @paramters        lstTriggerNew(List<AV_Study_Country__c>): List of newly created Study Site records.
                       
    * @return           void
    *********************************************************************************************************/   
    public static void updateSubjectFirstDose(List<AV_Study_Country__c> lstTriggerNew){
    Set<id>  setOfIDStudyCountryIsChecked = new Set<id>();
    Set<id>  setOfIDStudyIsChecked = new Set<id>();
    List<AV_Study__c>  setStudyToUpdate = new List<AV_Study__c>();
    try{
    for(AV_Study_Country__c eachStudySite : lstTriggerNew){
            setOfIDStudyCountryIsChecked.add(eachStudySite.AV_Study__c);
            //mapOfIDStudyCountryToIsChecked.put(eachStudySite.AV_Study_Country__c, false);
        }
    for(AV_Study__c eachStudy : [select id, AV_Last_Subject_First_Dose__c, (select id, AV_Last_Subject_First_Dose__c from Study_Countries__r) from AV_Study__c where id IN: setOfIDStudyCountryIsChecked] ){
            //mapOfStudyCountryToStudySite.put(eachStudyCountry.id, eachStudyCountry.Study_Sites__r);
            Boolean isAllChecked = true;
            for(AV_Study_Country__c eachStudyCountry : eachStudy.Study_Countries__r){
                if(eachStudyCountry.AV_Last_Subject_First_Dose__c)
                    continue;
                else{
                    isAllChecked = false;
                    break;
                }
            }
            if(isAllChecked){
            setOfIDStudyIsChecked.add(eachStudy.id);
            eachStudy.AV_Last_Subject_First_Dose__c = true;
            }else{
            eachStudy.AV_Last_Subject_First_Dose__c = false;    
            }
            setStudyToUpdate.add(eachStudy);            
        }
        
        if(!setStudyToUpdate.isEmpty())
            update setStudyToUpdate;
    }catch(Exception errorMessage){

            //logging errors
            AV_LOG_LogMessageUtility.logMessage(errorMessage, 'AV_StudyCountryTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'updateSubjectFirstDose', 'Error', TRUE); 
        }
    }
}