/********************************************************************************************************
* @author         Deloitte
* @description    This is the handler class for AV_ChecklistQuestionTrigger
* @date           11/9/2016
* 
* Modification Log:
* -------------------------------------------------------------------------------------------------------
* Developer                Date                   Modification ID      Description
* -------------------------------------------------------------------------------------------------------
* Deloitte                 11/9/2016                                   Initial version
*********************************************************************************************************/

public with sharing class AV_ChecklistQuestionTriggerHelper {

     /********************************************************************************************************
     * @author        Deloitte
     * @date          11/9/2016
     * @description   This is the constructor
     * @param         
     * @return        
     *********************************************************************************************************/
    public AV_ChecklistQuestionTriggerHelper() {        
    }

	 /********************************************************************************************************
	 * @author        Deloitte
	 * @date          11/9/2016
	 * @description   EXECUTE BEFORE UPDATE LOGIC
 	 * @param 		  oldcQuestionMap,newcQuestionMap
 	 * @return        void
	 *********************************************************************************************************/	
     public void OnBeforeUpdate(Map<ID, AV_Checklist_Questions__c> oldcQuestionMap, Map<ID, AV_Checklist_Questions__c> newcQuestionMap) {
     	checkDataEditBasedOnStatus(oldcQuestionMap, newcQuestionMap);
     	checkForActiveRuleConfig(oldcQuestionMap, newcQuestionMap);
     }

	/********************************************************************************************************
	* @author        Deloitte
	* @date          11/10/2016
	* @description   This method ensures that If the “Checklist Question Status” is “’Active or “Inactive” , 
	* 				 edits to the data fields is not allowed except: Checklist Question Status.
	* @param         Map<ID, AV_Checklist_Questions__c> oldcQuestionMap, Map<ID, AV_Checklist_Questions__c> newcQuestionMap
	* @return        void
	*********************************************************************************************************/
	public void checkDataEditBasedOnStatus(Map<ID, AV_Checklist_Questions__c> oldcQuestionMap, Map<ID, AV_Checklist_Questions__c> newcQuestionMap) {
		List<String> fieldList = new List<String>();
		fieldList.addAll(AV_Common_Utilities.retrieveAllFields(AV_Checklist_Questions__c.getSObjectType()));
		Boolean dataModified = false;
		Integer count = 0 ;
		Set<String> exceptionFieldSet = new Set<String> {AV_StaticConstants.CHECKLIST_QUESTION_STATUS_API_NAME,
															AV_StaticConstants.LASTMODIFIEDDATE_API_NAME,
															AV_StaticConstants.LASTMODIFIEDBYID_API_NAME};
		if( fieldList != null && !fieldList.IsEmpty() && newcQuestionMap != null && !newcQuestionMap.keySet().IsEmpty()) {
			for(Id cqId : newcQuestionMap.keySet()) {
				dataModified = false;
				if(oldcQuestionMap.get(cqId).AV_Checklist_Question_Status__c.equals(AV_StaticConstants.STATUS_ACTIVE) || 
							oldcQuestionMap.get(cqId).AV_Checklist_Question_Status__c.equals(AV_StaticConstants.STATUS_INACTIVE)) {
					count = 0;
					while (!dataModified && count < fieldList.size()) {
						String fld = fieldList[count];
						if(!exceptionFieldSet.contains(fld) && oldcQuestionMap.get(cqId).get(fld) != newcQuestionMap.get(cqId).get(fld)) {
							dataModified = true;
						}
						count++;
					}
					if(dataModified) {
						newcQuestionMap.get(cqId).addError(System.Label.AV_Update_On_Active_Inactive_Question_Error);
					}
				}
					
			}
		}
	}

	/********************************************************************************************************
	* @author        Deloitte
	* @date          11/9/2016
	* @description   This method checks for active Rule Configuration records of type Monitoring Activity. If exists,
	* 				 an Active Checklist Status must not be updated to Inactive.
	* @param         Map<ID, AV_Checklist_Questions__c> oldcQuestionMap, Map<ID, AV_Checklist_Questions__c> newcQuestionMap
	* @return        void
	*********************************************************************************************************/
	public void checkForActiveRuleConfig(Map<ID, AV_Checklist_Questions__c> oldcQuestionMap, Map<ID, AV_Checklist_Questions__c> newcQuestionMap) {
		Set<Id> cqIdSet = new Set<Id>();
		Map<Id,Boolean> cqRuleFlagMap = new Map<Id,Boolean>();
		if(newcQuestionMap != null && !newcQuestionMap.keySet().IsEmpty()) {
			for (Id cqId : newcQuestionMap.keyset()) {
				if(oldcQuestionMap.get(cqId).AV_Checklist_Question_Status__c.equals(AV_StaticConstants.STATUS_ACTIVE) &&
					newcQuestionMap.get(cqId).AV_Checklist_Question_Status__c.equals(AV_StaticConstants.STATUS_INACTIVE)) {
					cqIdSet.add(cqId);
				}
			}
		}
		if(cqIdSet != null && !cqIdSet.IsEmpty()) {
			cqRuleFlagMap = AV_SOQLQueryUtility_ChecklistQuestion.relatedActiveRuleConfigChecker(cqIdSet);
		}
		if(cqRuleFlagMap !=null && !cqRuleFlagMap.keySet().IsEmpty()) {
			for(Id cqId : cqRuleFlagMap.keySet()) {
				newcQuestionMap.get(cqId).addError(System.Label.AV_Status_Change_Error_Message);
			}
		}
	}
}