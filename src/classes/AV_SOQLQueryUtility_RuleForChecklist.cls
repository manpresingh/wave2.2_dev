/*********************************************************************************************************
 * Class Name   : AV_SOQLQueryUtility_RuleForChecklist
 * Description  : Generic service class for SOQL queries on AV_Rule_for_Checklist__c.
 * Created By   : Deloitte
 * Created On   : 11/11/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                11/11/2016                                   Initial version
*/

public with sharing class AV_SOQLQueryUtility_RuleForChecklist{

   /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-11
   * @description      gets data from Rule for checklist and questions object using type values as filter
   * @Paramters        list<String> containing unique values of type values to be queried
   * @return           list<AV_Rule_for_Checklist__c>
   *********************************************************************************************************/
   public static list<AV_Rule_for_Checklist__c> getRuleForChecklistRecords(set<String> MonitoringTypeLst){
       //variable to store 'Active' string value for filtering active records
       String activeStatus = Label.AV_Active;
       //logic to get recordtypeid for monitoring record type in rule config object using its developer name, if the value is not present in static constant already
       if(String.isBlank(AV_StaticConstants.monActivityRuleConfigRTid))
           AV_StaticConstants.monActivityRuleConfigRTid = AV_SOQLQueryUtility_RecordType.getRecordTypeId('AV_Rule_Configuration__c', 'AV_Monitoring_Activity_Rule_Configuration');
       //query to get all the checklist questions related to the monitoring activity type via rule for checklist junction object
       list<AV_Rule_for_Checklist__c> chklstRuleLst = [select Id, 
                                                              AV_Checklist_Question__c, 
                                                              AV_Rule_Configuration__c, 
                                                              AV_Sequence__c, 
                                                              AV_Rule_Configuration__r.AV_Monitoring_Activity_Type__c, 
                                                              AV_Checklist_Question__r.AV_Topic__c,
                                                              AV_Checklist_Question__r.AV_Question__c
                                                       from AV_Rule_for_Checklist__c 
                                                       where AV_Rule_Configuration__r.AV_Status__c = :activeStatus and 
                                                             AV_Rule_Configuration__r.RecordTypeId = :AV_StaticConstants.monActivityRuleConfigRTid and 
                                                             AV_Rule_Configuration__r.AV_Monitoring_Activity_Type__c in :MonitoringTypeLst
                                                       order by AV_Sequence__c];
       return chklstRuleLst;
   }

/********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-11
   * @description      Returns the list of AV_Rule_for_Checklist__c records with id and sequence
   * @Paramters        none
   * @return           list<AV_Rule_for_Checklist__c>
   *********************************************************************************************************/
   public static List<AV_Rule_for_Checklist__c> getSequenceNumber(){
    List<AV_Rule_for_Checklist__c> lst_ChecklistResponse =[select id,AV_Rule_Configuration__c,
                                                               AV_Sequence__c from AV_Rule_for_Checklist__c ];

          return lst_ChecklistResponse;                                                     
   }
}