/********************************************************************************************************
* @author       Deloitte
* @description  This test class is the framework used to generate test data to be used by the other test classes
* @date         2015-06-29
* @group        Message Logs - Test Classes
*********************************************************************************************************/
@isTest
public class AV_LOG_TestDataUtility {
   
    /********************************************************************************************************
    * @author       Karolinski Stephane
    * @date         2015-08-17
    * @description  This method generates a Log Message Custom setting with the parameters provided as input
    * @param        error (Boolean): indicates if the error logs must be logged
    * @param        warning (Boolean): indicates if the warning logs must be logged
    * @param        info (Boolean): indicates if the info logs must be logged
    * @param        debug (Boolean): indicates if the debug logs must be logged
    * @param        logPurge (Boolean): the number of day before the log messages must be deleted
    * @return       void
    *********************************************************************************************************/
    public static AV_LOG_LogMessageSettings__c generateErrorMgtCustomSetting(Boolean error, Boolean warning , Boolean info, Boolean debug, Integer logPurge)    
    {
       //insert custom setting
       AV_LOG_LogMessageSettings__c setg = new AV_LOG_LogMessageSettings__c();
       setg.Name = 'Default';
       setg.AV_LOG_Debug__c = debug;
       setg.AV_LOG_Error__c = error;
       setg.AV_LOG_Info__c = info;
       setg.AV_LOG_Warning__c = warning;
       setg.AV_LOG_LOG_Purge__c = logPurge;
       insert setg;
        
       return setg;
    }
 
    /********************************************************************************************************
    * @author       Karolinski Stephane
    * @date         2015-08-17
    * @description  This method generates a list of test accounts
    * @param        nrOfAccounts (integer): the number of accounts to be created
    * @param        nameRoot (String): the rootname of the accounts being generated
    * @param        executeInsert (Boolean): True of the DML insert must be executed or not, False otherwise
    * @return       List<Account>: the list of generated accounts
    *********************************************************************************************************/
    public static List<Account> generateAccounts(integer nrOfAccounts, String nameRoot, Boolean executeInsert)
    {
        List<Account> accList = new List<Account>();
        for (integer i=1; i<= nrOfAccounts; i++)
        {
            Account acc = new Account();
            acc.Name = nameRoot + ' - ' + String.valueOf(i);
            
            accList.add(acc);
        }
        if (executeInsert)
            insert accList;
        
        return accList;
    }

    /********************************************************************************************************
    * @author       Karolinski Stephane
    * @date         2015-08-17
    * @description  This method generates a list of test contacts
    * @param        nrOfContacts (integer): the number of contacts to be created
    * @param        nameRoot (String): the rootname of the contacts being generated
    * @param        linkToAccount (Account): the account to which the contact must be linked to
    * @param        executeInsert (Boolean): True of the DML insert must be executed or not, False otherwise
    * @return       List<Contact>: the list of generated contacts
    *********************************************************************************************************/
    public static List<Contact> generateContacts(integer nrOfContacts, String nameRoot, Account linkToAccount, Boolean executeInsert)
    {
        List<Contact> ctcList = new List<Contact>();
        for (integer i=1; i <= nrOfContacts; i++)
        {
            Contact ctc = new Contact();
            ctc.AccountId = linkToAccount.Id;
            ctc.LastName = nameRoot + ' - ' + String.valueOf(i);
            ctc.AV_Personnel_Number__c = string.valueof(111000+i+1);
            ctcList.add(ctc);
        }
        
        if (executeInsert)
            insert ctcList;
        
        return ctcList;
    }

    /********************************************************************************************************
    * @author       Karolinski Stephane
    * @date         2015-08-17
    * @description  This method retireves the logMessage specified by the provided SFDC Id
    * @param        logId (Id): the SFDC Id of the Log message to be retrieved
    * @return       AV_LOG_LogMessage__c: the Queried Log Message
    *********************************************************************************************************/ 
    public static AV_LOG_LogMessage__c getInsertedLog(Id logId){
       AV_LOG_LogMessage__c insertedMsg = [SELECT AV_LOG_Age__c, AV_LOG_Debug_Level__c, Id, AV_LOG_LOG_Code__c, AV_LOG_Message__c, Name, AV_LOG_Reference_Id__c, AV_LOG_Reference_Info__c, AV_LOG_Source__c, AV_LOG_Source_Function__c, AV_LOG_Stack_Trace__c, AV_LOG_Timer__c, CreatedDate, AV_LOG_Integration_Payload__c 
                                        FROM AV_LOG_LogMessage__c
                                        WHERE Id = :logId
                                       LIMIT 1];
        return insertedMsg;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   This method creates an instance of a Checklist Question record.
    * @param         String status, String question, String topic, String answerOptions
    * @return        AV_Checklist_Questions__c
    *********************************************************************************************************/
    public static AV_Checklist_Questions__c createChecklistQuestion(String status, String question, String topic, String answerOptions) {
        return new AV_Checklist_Questions__c(AV_Checklist_Question_Status__c = status,
                                            AV_Question__c = question,
                                            AV_Topic__c = topic,
                                            AV_Answer_Options__c = answerOptions);
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   This method returns an instance of a Test user for a given profile
    * @param         String profileName, String alias, String roleName
    * @return        User
    *********************************************************************************************************/
    public static User createTestUser(String profileName, String alias, String roleName) {
        List<Profile> prof = [SELECT Id FROM Profile WHERE Name = :profileName LIMIT 1];
        User usr = null;
        if(!prof.isEmpty()) {
            usr = new User(Alias = alias, Email= alias+'_testuser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', 
                            LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = prof[0].Id,TimeZoneSidKey='America/Los_Angeles', 
                            UserName= alias+'_testuser@testorg.com');
            if(String.isNotBlank(roleName))
            {      
                List<UserRole> rl = [SELECT Id FROM UserRole WHERE DeveloperName=:roleName LIMIT 1 ];
                if(!rl.isEmpty()){
                    usr.UserRoleId = rl[0].Id;
                }   
            }
        }
        return usr;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   This method creates an instance of a Monitoring Activity Rule Configuration
    * @param         String type, String status
    * @return        AV_Rule_Configuration__c
    *********************************************************************************************************/
    public static AV_Rule_Configuration__c createMonitoringRuleConfiguration(String mtype, String status) {
        AV_Rule_Configuration__c ruleConfig = new AV_Rule_Configuration__c();
        Id rtypeId = AV_Common_Utilities.fetchRecordTypeIdByDeveloperName(AV_StaticConstants.RULECONFIG_OBJ_NAME, AV_StaticConstants.RULECONFIG_MONITORING_RTYPE_NAME);
        if(String.isNotBlank(rtypeId)) {
            ruleConfig =  new AV_Rule_Configuration__c( AV_Monitoring_Activity_Type__c = mtype,
                                            AV_Status__c = status,
                                            RecordtypeId = rtypeId
                                        );
        }
        return ruleConfig;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   This method creates an instance of Rule for Checklist record
    * @param         Id ruleConfigId, Id checklistQuestionId
    * @return        AV_Rule_for_Checklist__c
    *********************************************************************************************************/
    public static AV_Rule_for_Checklist__c createRuleForChecklist(Id ruleConfigId, Id checklistQuestionId, Integer seq) {
        return new AV_Rule_for_Checklist__c (AV_Checklist_Question__c = checklistQuestionId, AV_Rule_Configuration__c = ruleConfigId,
                                            AV_Sequence__c = seq);
    }
}