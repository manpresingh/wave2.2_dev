/********************************************************************************************************
* @author         Deloitte
* @description    Generic service class for SOQL queries on AV_Checklist_Questions__c
* @date           11/9/2016
* 
* Modification Log:
* -------------------------------------------------------------------------------------------------------
* Developer                Date                   Modification ID      Description
* -------------------------------------------------------------------------------------------------------
* Deloitte                 11/9/2016                                   Initial version
*********************************************************************************************************/
public with sharing class AV_SOQLQueryUtility_ChecklistQuestion {

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/9/2016
    * @description   This method retrieves all the Rule Configuration records related to the current 
    *                Checklist Question via child Rule Checklist records 
    * @param         Set<Id> cqIdSet
    * @return        Map<Id,Boolean>
    *********************************************************************************************************/
    public static Map<Id,Boolean> relatedActiveRuleConfigChecker(Set<Id> cqIdSet) {
        Map<Id,Boolean> cqRuleFlagMap = new Map<Id,Boolean>();
        if(cqIdSet !=null && !cqIdSet.IsEmpty()) {
            for(AV_Rule_for_Checklist__c rcheck : [SELECT Id, AV_Checklist_Question__c  
                                                    FROM AV_Rule_for_Checklist__c
                                                    WHERE AV_Checklist_Question__c IN :cqIdSet
                                                    AND AV_Rule_Configuration__r.RecordType.DeveloperName = :AV_StaticConstants.RULECONFIG_MONITORING_RTYPE_NAME 
                                                    AND AV_Rule_Configuration__r.AV_Status__c = :AV_StaticConstants.STATUS_ACTIVE]) {
                if(String.isNotBlank(rcheck.AV_Checklist_Question__c)) {
                    cqRuleFlagMap.put(rcheck.AV_Checklist_Question__c, true);
                }
            }
        }
        return cqRuleFlagMap;
    }   

}