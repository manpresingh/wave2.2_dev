/********************************************************************************************************
* @author         Deloitte
* @description    Common Utility class for reusable functions.
* @date           2016-03-30
*********************************************************************************************************
* Modification Log:  
* -------------------------------------------------------------------------------------------------------
* Developer                 Date                   Modification ID      Description 
* -------------------------------------------------------------------------------------------------------
* Deloitte               03/11/2016                                    Initial version
*/
 
public without sharing class AV_Common_Utilities {

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   Retrieves the picklist Values based on Objcet and Field Name
    * @Paramters     Protocol and Ammendment Approval RecordId.
    * @return        AV_Protocol_Approval_and_Amendment__c
    *********************************************************************************************************/
    public Static List<String> getPicklistValues(String objectName, String fieldName, String firstValue) {
        List<String> pickValues = new List<String>(); //new list for holding all of the picklist options
        if (String.isNotBlank(firstValue)) { //if there is a first value being provided
            pickValues.add(firstValue); //add the first option
        }
        Schema.sObjectType sobject_type = Schema.getGlobalDescribe().get(objectName); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry eachVal : pick_list_values) { //for all values in the picklist list
            pickValues.add(eachVal.getValue()); //add the value and label to our final list
        }
        return pickValues; //return the List
    }
    
    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   Converts an email template to an HTML body using a dummy contact which will be used to replace the content
    *                of the email body or to create attachments.
    * @Paramters     Map of What Id, List of Templete Ids.
    * @return        Map of What id, map of template id, String of HTML body
    *********************************************************************************************************/
    public static Map<Id, Map<id, String>> createHTMLAttachment(Map<Id, List<Id>> mapObjIdAndTemplateId, Boolean isRecipient){

        Savepoint sp = Database.setSavepoint();
        Map<Id, Map<id, String>> mapObjIdAndHTMLbody = new Map<Id, Map<id, String>>();
        List<Messaging.SingleEmailMessage> lstmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        contact dummyContact = new Contact(lastName = 'Test',AV_Personnel_Number__c='123455', email = 'invalid@emailaddr.test');
        if(dummyContact != null){
            insert dummyContact ; //Inserting a dummy contact to generate Email Attachment.
        }
       
        for(Id eachObjId : mapObjIdAndTemplateId.keySet()){
            for(Id eachTemplateId : mapObjIdAndTemplateId.get(eachObjId)){
                Messaging.reserveSingleEmailCapacity(1);
                mail = prepareEmailBody(eachObjId, eachTemplateId, null, null, dummyContact.Id, null,isRecipient, null);                
                lstmails.add(mail); 
            }         
        }
        if(!lstmails.isEmpty()){
            try{
                Map<Id,Messaging.SendEmailResult> whatIDToResultMap = new Map<Id,Messaging.SendEmailResult>();
                List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(lstmails, FALSE);
                /*
                    Iterate through send Email result and prepare Map with 
                    WhatId and Send Email result.
                 */
                Integer i=0;
                for(Messaging.SendEmailResult sendEmailResult: sendEmailResults){
                    whatIDToResultMap.put(lstmails.get(i).getWhatId(),sendEmailResult);    
                    i++;
                }
                
                /*
                    Add to Map only if Email deliverability is Successfull.
                 */
                for(Messaging.SingleEmailMessage eachEmail : lstmails){
                    if(whatIDToResultMap.containsKey(eachEmail.getWhatId()) && 
                        whatIDToResultMap.get(eachEmail.getWhatId()).isSuccess()){                        
                        String htmlText = eachEmail.htmlBody != null ? eachEmail.htmlBody : eachEmail.plainTextBody;
                        if(mapObjIdAndHTMLbody.containsKey(eachEmail.whatId)){
                            mapObjIdAndHTMLbody.get(eachEmail.whatId).put(eachEmail.templateId, htmlText);
                        }else{
                            mapObjIdAndHTMLbody.put(eachEmail.whatId, new map<id, String>{eachEmail.templateId => htmlText});
                        }
                    }
                }
            } catch(Exception commonException){
                AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_Common_Utilities', AV_StaticConstants.EXC_TYPE_APP, 'sendSiteSelectionEmail', 'Error', TRUE); 
            }
        }
        
        Database.rollback(sp);
        return mapObjIdAndHTMLbody;
    }
    

    // Map variable to Constants key and Value
    public static Map <String, String> applicationConstantsMap = new Map <String, String> ();

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to fetch application constants from 
    *                Generic Constants Custom Setting.
    * @Paramters     none
    * @return        void
    *********************************************************************************************************/
    public static void populateApplicationConstantsMap() {
        // Fetching all values of Generic Constants Custom Setting
        Map<String, AV_Generic_Constants__c> allValuesMap = AV_Generic_Constants__c.getAll();
        if(applicationConstantsMap.isEmpty()){
            for (String eachKey : allValuesMap.keySet()) {
                applicationConstantsMap.put(eachKey, allValuesMap.get(eachKey).AV_Constant_Value__c);
            }
        }
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to set empty dates received from vf page 
    *                to null.
    * @Paramters     date to be nullified
    * @return        nullified date
    *********************************************************************************************************/
    public static Date dateNullifier(Date nullifyDateVar) {
        Date correctedDateVar = null;
        if(nullifyDateVar != null){
            if(!Label.AV_Null_Date_Year_Dummy_String.equals(String.valueOf(nullifyDateVar.year()))) {
                correctedDateVar = nullifyDateVar;
            }
        }
        return correctedDateVar;
    }    

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to fetch country code and name from 
    *                Country Custom Setting.
    * @Paramters     none
    * @return        map<name, code>
    *********************************************************************************************************/
    public map<string, string> populateCountryCodeNameMap() {
        map<string, string> countryCodeNameMap = new map<string, string>();
        //fetching all the country code and name records from the custom setting
        list<AV_Country__c> countryCodeNameLst = AV_Country__c.getall().values();
        if(countryCodeNameLst!=null && !countryCodeNameLst.isEmpty()){
            for(AV_Country__c rec: countryCodeNameLst){
                if(rec.AV_Country_Name__c != null){
                    if(rec.name != null){
                        countryCodeNameMap.put(rec.AV_Country_Name__c, rec.name);
                    }
                }
            }
        }
        return countryCodeNameMap;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to return true/false to execute particular trigger or not
    * @Paramters     profieId of user, object needed access to
    * @return        Boolean for allowing or not
    *********************************************************************************************************/
    public static Boolean canProfileBeBypassed(String profileId, String objectName) {
        Boolean isBypass = FALSE;
        // Fetching all values of AV_Bypass_Rules_and_Triggers__c Custom Setting
        AV_Bypass_Rules_and_Triggers__c triggerRecord = AV_Bypass_Rules_and_Triggers__c.getInstance(profileId);
        if(String.isNotBlank(triggerRecord.AV_Object__c)){
            Set<String> valuesSet = convertStringToSet(triggerRecord.AV_Object__c, AV_StaticConstants.CONSTANT_COMMA); 
            if(valuesSet.contains(objectName)){
                isBypass = triggerRecord.AV_Triggers_Disabled__c;
            }
        }
        return isBypass;
    } 

    public static Map <String, Map <String, RecordTypeInfo>> recordTypeInfoMapVar = new Map <String, Map <String, RecordTypeInfo>>();
    
    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   Generic Method to return Record Type Id based on 
    *                recordType Name and Object name
    * @Paramters     ObjectName and recordType Name
    * @return        Id for the recordType
    *********************************************************************************************************/
    public static Id fetchRecordTypeID(String objectNameVar, String recordTypeName) {

        if (recordTypeInfoMapVar.isEmpty() || (!recordTypeInfoMapVar.isEmpty() && 
                                            !recordTypeInfoMapVar.containskey(objectNameVar))) {
            //Retrieve the describe result for the desired object
            DescribeSObjectResult sObjResult = Schema.getGlobalDescribe().get(objectNameVar).getDescribe();
            //Generate a map of tokens for all the Record Types for the desired object
            recordTypeInfoMapVar.put(objectNameVar, sObjResult.getRecordTypeInfosByName());
        }
        return recordTypeInfoMapVar.get(objectNameVar).get(recordTypeName).getRecordTypeId();
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   Generic Method to convert String to Set
    * @Paramters     stringVar and dilimeter
    * @return        Set<String>
    *********************************************************************************************************/
    public static Set<String> convertStringToSet(String stringVar, String dilimeter) {
        Set<String> convertedSet = new Set<String>();
        if (String.isNotBlank(stringVar)) {
            for(String eachStr : stringVar.split(dilimeter)){
                convertedSet.add(eachStr.trim());
            }
        }
        return convertedSet;
    }

    public static Map <String, String> templateIdMap = new Map <String, String>();
    
    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   Generic Method to Fetch Template Name for Event Date Notifications
    * @Paramters     ObjectName and recordType Name
    * @return        template ID
    *********************************************************************************************************/
    public static String fetchTemplateIdForEventDates(String templateName) {

        if (templateIdMap.isEmpty() || (!templateIdMap.isEmpty() && 
                                        !templateIdMap.containskey(templateName)) ) {
            
            Map<String, AV_EventDates_EmailTemplates_ET__c> allValuesMap = AV_EventDates_EmailTemplates_ET__c.getAll();
            for (String eachKey : allValuesMap.keySet()) {
                templateIdMap.put(eachKey, allValuesMap.get(eachKey).AV_Email_Template_ID__c);
            }
        }
        return templateIdMap.get(templateName);
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   Generic method to prepare and return SingleEmailMessage.
    *                Email body will be set based on either template Id / Email Body
    * @Paramters     whatId, templateId, lstEmailIds, emailBody
    * @return        Messaging.SingleEmailMessage.
    *********************************************************************************************************/
    public Static Messaging.SingleEmailMessage prepareEmailBody(String whatId, String templateId, 
                                                            List<String> lstEmailIds, String emailBody, 
                                                            Id targetObjectId, String subject,
                                                            Boolean isRecipient, List<String> lstCCAddress) {

        Messaging.SingleEmailMessage emailvar = new Messaging.SingleEmailMessage();
        emailvar.setSaveAsActivity(false);
        emailvar.setWhatId(whatId);
        if(targetObjectId != null){
            emailvar.setTargetObjectId(targetObjectId);
        }
        emailvar.setTreatTargetObjectAsRecipient(isRecipient);
        emailvar.setOrgWideEmailAddressId(Label.AV_Organization_Wide_Email_Address); 
        if(String.isNotBlank(templateId)){
            emailvar.setTemplateId(templateId);
        }
        if(lstEmailIds != null && !lstEmailIds.isEmpty()){
            emailvar.setToAddresses(lstEmailIds);    
        }
        if(lstCCAddress != null){
            emailvar.setCcAddresses(lstCCAddress); 
        }  
        if(String.isNotBlank(emailBody)){
            emailvar.setSubject(subject);
            emailvar.setHtmlBody(emailBody);
        }

        return emailvar;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to create email attachment for individual template.
    * @Paramters     Attachment Body, Studt Site record, type of attachment and lab name.
    * @return        Messaging.Emailfileattachment
    *********************************************************************************************************/
    public static Messaging.Emailfileattachment createSiteEmailAttachment(String attachmentBody, AV_Study_Site__c eachStudySite, String attType, Map<Id, String> mapStudySiteLabName){
        Messaging.Emailfileattachment emailAttachment = new Messaging.Emailfileattachment();
        if(mapStudySiteLabName != null && mapStudySiteLabName.containsKey(eachStudySite.id)){
            if(mapStudySiteLabName.get(eachStudySite.id) != null && mapStudySiteLabName.get(eachStudySite.id) != ''){
                attachmentBody = attachmentBody.replace('Study_Site_LabName', mapStudySiteLabName.get(eachStudySite.id));
            }else{
                attachmentBody = attachmentBody.replace('Study_Site_LabName', '');
            }
        }
        emailAttachment.setFileName(eachStudySite.AV_Protocol_Number__c+'_'+eachStudySite.AV_Study_Site_Reference__c+'_'+eachStudySite.AV_Principal_Investigator_RelatedList__c+'_'+attType+'.doc');
        emailAttachment.setBody(Blob.valueOf(attachmentBody));
        emailAttachment.setContentType('application/msword; charset=UTF-8');

        return emailAttachment;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to check the mandatory field for the email to send.
    * @Paramters     Study Site record
    * @return        Boolean
    *********************************************************************************************************/
    public static Boolean checkMandatoryFields(AV_Study_Site__c eachStudySite,Map<Id, String> mapLabName){
        Boolean isEmailCheck = true;
        
        if(String.isNotBlank(eachStudySite.AV_Protocol_Number__c) &&
            String.isNotBlank(eachStudySite.AV_Compound_Code_ET__c) &&
            String.isNotBlank(eachStudySite.AV_Principal_Investigator__c) &&
            String.isNotBlank(eachStudySite.AV_Study_Country__r.AV_Study__r.AV_Protocol_Title__c)){
            
            Boolean isFDCEUAtt = false; //Boolean to check if FCD EU attachment should be attach to the email.
            Boolean isFDCAtt = false; //Boolean to check if FCD attachment should be attach to the email
            Boolean isOUSIIAAtt = false; //Boolean to check if IIA attachment should be attach to the email
            
            if(AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_Countries__c.contains(eachStudySite.AV_Country_Name__c)){
                isFDCEUAtt = AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_FDC_EU_Attachment__c;
                isFDCAtt = AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_FDC_Attachment__c;
                isOUSIIAAtt = AV_Site_Notification_Template_Selection__c.getValues('EU Group').AV_IIA_Attachment__c;
            }else if(AV_Site_Notification_Template_Selection__c.getValues('US').AV_Countries__c.contains(eachStudySite.AV_Country_Name__c)){
                isFDCEUAtt = AV_Site_Notification_Template_Selection__c.getValues('US').AV_FDC_EU_Attachment__c;
                isFDCAtt = AV_Site_Notification_Template_Selection__c.getValues('US').AV_FDC_Attachment__c;
                isOUSIIAAtt = AV_Site_Notification_Template_Selection__c.getValues('US').AV_IIA_Attachment__c;
            }else{
                isFDCEUAtt = AV_Site_Notification_Template_Selection__c.getValues('Others').AV_FDC_EU_Attachment__c;
                isFDCAtt = AV_Site_Notification_Template_Selection__c.getValues('Others').AV_FDC_Attachment__c;
                isOUSIIAAtt = AV_Site_Notification_Template_Selection__c.getValues('Others').AV_IIA_Attachment__c;
            }
            system.debug('@@@@ the booleans are '+isFDCEUAtt+' AND '+isFDCAtt+' AND '+isOUSIIAAtt);
            if(isFDCAtt && (String.isBlank(eachStudySite.AV_Primary_Facility__c) || String.isBlank(eachStudySite.AV_PI_Email_Address_ET__c))){
               
                isEmailCheck = false;     
            }    
            if(isFDCEUAtt && String.isBlank(eachStudySite.AV_PI_Street_Address_ET__c)){
                system.debug('@@@@@@@@ Entered in FDC EU missing field');
                isEmailCheck = false;
            }
            if(isOUSIIAAtt && (String.isBlank(eachStudySite.AV_PI_Street_Address_ET__c) || String.isBlank(eachStudySite.AV_Primary_Facility__c) || !mapLabName.containsKey(eachStudySite.id))){
                system.debug('@@@@@@@@ Entered in IIA missing field'+mapLabName.get(eachStudySite.id));
                isEmailCheck = false;
            }
        }else{
            isEmailCheck = false;
        }
        return isEmailCheck;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to populate EventDate External ID's
    * @Paramters     actionItem(Study,Study Country,Study Site),EventNumber,ExternalID
    * @return        String
    *********************************************************************************************************/
    public static String populateEventDateExternalID(String actionItem,string EventNumber,string ExternalID){
        string eventDateExternalID = '';
        if(ExternalID !=null && ExternalID!=''){
            eventDateExternalID = actionItem +Label.AV_Pipe_Symbol+ EventNumber +Label.AV_Pipe_Symbol+ ExternalID;
        }
        else
        {
            eventDateExternalID = '';
        }
        return eventDateExternalID;
    }     

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used delete Event dates from AV_EventAssignmentController
    * @Paramters     List of Event date records.
    * @return        String
    *********************************************************************************************************/
    public static void deleteEventDates(List<AV_Event_Date__c> lstOfEventDates){
        DELETE lstOfEventDates;
    }     

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-30
    * @description   This method is used to populate CPA External ID's
    * @Paramters     actionItem(Study,Study Country,Study Site),EventNumber,ExternalID, softDeleteCheck
    * @return        String
    *********************************************************************************************************/
    public static String populateCPAExternalID(string actionItem,string CPUniqueID,string LevelUniqueID, string roleUID, Boolean isSoftDeleteRecord){
        String CPAExternalID;
        if(isSoftDeleteRecord){
            CPAExternalID = actionItem + Label.AV_SoftDelete_Keyword_Prefix+DateTime.now().getTime() ;
        }
        else{
            DateTime currentGMTTime = DateTime.now();
            String formattedCurrentGMTTime = currentGMTTime.formatGMT(AV_StaticConstants.EXTERNALID_DATE_FORMAT);
            CPAExternalID = actionItem +Label.AV_Pipe_Symbol+ CPUniqueID +Label.AV_Pipe_Symbol+ LevelUniqueID +Label.AV_Pipe_Symbol + roleUID + Label.AV_Pipe_Symbol + formattedCurrentGMTTime;
        }

        return CPAExternalID;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-06-21
    * @description   This method is used to populate External ID for Protocol Amendment Approval records
    *                ex: "REGULATORY|0|M13-537|US"
    * @paramters     approvalRecordType(String): Record type name of Approval Record.
    * @paramters     amendmentNo(String): Amendment Number for Amendment/ Protocol.
    * @paramters     relatedRecordUniqueId(String): Record Id of related (Study Country/ Study Site).
    * @paramters     existingExternalID(String): Existing external id for Approval record.
    * @paramters     isSoftDelete(Boolean): Is record soft delete.
    * @return        String : External for Approval Record.
    *********************************************************************************************************/
    public static String populateApprovalRecordExternalID(String approvalRecordType, String amendmentNo, 
                                                    String relatedRecordUniqueId, String existingExternalID, 
                                                    Boolean isSoftDelete) {
        String approvalRecordExternalID = '';
        try{
            if(isSoftDelete && String.isNotBlank(existingExternalID)){
                approvalRecordExternalID = existingExternalID + Label.AV_SoftDelete_Keyword_Prefix + DateTime.now().getTime();
            } else if(String.isNotBlank(approvalRecordType) && 
                    String.isNotBlank(amendmentNo) && String.isNotBlank(relatedRecordUniqueId)){
                approvalRecordExternalID = approvalRecordType +Label.AV_Pipe_Symbol+ amendmentNo +Label.AV_Pipe_Symbol+ relatedRecordUniqueId;
                approvalRecordExternalID = approvalRecordExternalID.toUpperCase(approvalRecordType);
            }
        } catch(Exception commonException){
            AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_Common_Utilities', AV_StaticConstants.EXC_TYPE_APP, 'populateApprovalRecordExternalID', 'Error', TRUE); 
        }
        return approvalRecordExternalID;
    }  
    
    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-10-18
    * @description   This method is used to fetch sorted country list from custom setting av_country__c    * 
    * @return        list<AV_Country__c>
    *********************************************************************************************************/     
    public static List<AV_Country__c> getCountryCodeNames() {    
      List<AV_Country__c> countryCodeNameLst = [select name,AV_Country_Name__c from av_country__c order by AV_Country_Name__c asc];
      return countryCodeNameLst;
    }
    
    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/10/2016
    * @description   Retrieve all the fields of the object.
    * @param         Schema.sObjectType obj
    * @return        Set<String>
    *********************************************************************************************************/
    public static Set<String> retrieveAllFields(Schema.sObjectType obj) {
        Set<String> fieldset = new Set<String>();
        if(obj != null) {
            fieldset = obj.getDescribe().fields.getMap().keySet();
        }
        return fieldset;
    }  

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   Retrieve the recordtypeId using object name and recordtype developername
    * @param         String objectName, String rtypeDevelopername
    * @return        Id
    *********************************************************************************************************/
    public static Id fetchRecordTypeIdByDeveloperName(String objectName, String rtypeDevelopername) {
        Id rtypeId = null;
        List<Recordtype> rtype = [SELECT Id FROM Recordtype WHERE SObjectType = :objectName AND DeveloperName = :rtypeDevelopername
                                    LIMIT 1];
        if(!rtype.isEmpty()) {
            rtypeId = rtype[0].Id;
        }
        return rtypeId;
    }      
    
}