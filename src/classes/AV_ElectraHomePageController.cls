/* @description    AV_ElectraHomePageController used to show Study Release for the logged in user. 

* @date           2016-11-08
*********************************************************************************************************

 * Modification Log:  
 * ------------------------------------------------------------------------------------------------------
 * Developer                Date            Modification ID             Description 
 * ------------------------------------------------------------------------------------------------------
 * Dilowar Hussain             2016-11-08                                   Initial version
*/
public with sharing class AV_ElectraHomePageController{
    //List containing all the study Release name for the logged in user 
    private static List<AV_Study_Releases__c> studyReleaseItems{
    set;
    get;
    }
    public String showContent { get; set; }
    //Convert 18 digit user id to 15 digit id
    private static String loggedInUserId = String.valueOf(UserInfo.getUserID()).substring(0, 15);
   
    public AV_ElectraHomePageController(){
     getshowContent();  
    }
    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-11-08
    * @description   Initialze getStudyReleaseDetails method will execute on Home Page load,displaying all the active study Releases                     
    * @Paramters     none
    * @return        home-page study Releases
    *********************************************************************************************************/ 
    
    public static List<AV_Study_Releases__c> getStudyReleaseDetails()
    {
       try{
            studyReleaseItems = [select Name,AV_Protocol_Number__c,AV_Protocol_Number__r.name, AV_Disclosure_Type__c,AV_Study_Release_Type__c,AV_Compound__c,AV_Study_Phase__c,AV_Recruitment_Status__c, LastModifiedDate, AV_Related_Study_Owner__c, AV_Related_Study_Owner__r.name, AV_Status__c from AV_Study_Releases__c where AV_Status__c in ('Draft', 'In Process','In Approval', 'Approved', 'Generated XML')and AV_Study_Release_Owner__r.AV_Active_Inactive__c = 'Active' and AV_Study_Release_Owner__r.av_user__c != null and AV_Study_Release_Owner__r.AV_user__c = :loggedInUserId ORDER BY LastModifiedDate DESC];        
          } catch(Exception commonException){
            AV_LOG_LogMessageUtility.logMessage(commonException,'AV_ElectraHomePageController', AV_StaticConstants.EXC_TYPE_APP, 'getStudyReleaseDetails', 'Error', TRUE);
        }  
        return studyReleaseItems ;
    }
    
   public String getshowContent(){
      
       try{
           getStudyReleaseDetails();
           if(studyReleaseItems.size()>0){
               showContent = 'true';  
           }else{
               showContent = 'false';  
           }
       }catch(Exception commonException){
          AV_LOG_LogMessageUtility.logMessage(commonException,'AV_ElectraHomePageController', AV_StaticConstants.EXC_TYPE_APP, 'getShowContent', 'Error', TRUE);
       }
       return showContent ;
   }
}