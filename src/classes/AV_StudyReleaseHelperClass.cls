/* 
 * Class Name   : AV_StudyReleaseHelperClass
 * Description  : 
 * Created By   : Deloitte
 * Created On   : 11/07/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               11/07/2016                                    Initial version
 */



public with sharing class AV_StudyReleaseHelperClass {
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       1. Update Owner field that matches the "Study Release Owner" contact on Update
                       2. Error on edit record when status is Approved/Generated XML
   * @Paramters        List<AV_Study_Releases__c>:Trigger.New 
                       Map<Id, AV_Study_Releases__c>: Trigger.newMap
                       Map<Id, AV_Study_Releases__c>: Trigger.oldMap
   * @return           void
   *********************************************************************************************************/
    public static void OnBeforeUpdate (List<AV_Study_Releases__c> newValue, Map<Id, AV_Study_Releases__c> newMapValue, Map<Id, AV_Study_Releases__c> oldMapValue){
        AV_Study_Releases__c oldsr = new AV_Study_Releases__c();
        Map<Id,Contact> UpdateOwnerResultMap = new Map<Id,Contact>();
        Set<Id> listContactIds = new Set<Id>();
        for(AV_Study_Releases__c studyRelease : newValue){//list all contact Ids of study releases whose study release owner is changed and status is Draft/In process
            oldsr = oldMapValue.get(studyRelease.Id);
            if(oldsr.AV_Study_Release_Owner__c != studyRelease.AV_Study_Release_Owner__c && studyRelease.AV_Study_Release_Owner__c!= Null){
                listContactIds.add(studyRelease.AV_Study_Release_Owner__c);
            }                      
        }
        if(!listContactIds.isEmpty()){
            UpdateOwnerResultMap = AV_SOQLQueryUtility_StudyReleases.getContactForOwnerChange(listContactIds);
        }
        Boolean showerror = False;
        for (AV_Study_Releases__c sr : newValue){
            oldsr = oldMapValue.get(sr.Id);
            Schema.SObjectType sobj = sr.getSObjectType();
            List<String> updatedFields = new List<String>();
            Map<String, Schema.SObjectField> fields = sobj.getDescribe().Fields.getMap();
            for (String fieldName : fields.keySet()) {
                if((oldsr.AV_Status__c == AV_StaticConstants.Approved || oldsr.AV_Status__c == AV_StaticConstants.Generated_XML)){
                    if(fieldName != 'AV_Status__c' && (sr.get(fieldName) != oldsr.get(fieldName))){
                        showerror = True;
                    } 
                }
            }
            if (showerror == True){
                newMapValue.get(sr.Id).addError(Label.AV_Study_Releases_Validation_Error);
            }
            //Update owner
            if(listContactIds.contains(sr.AV_Study_Release_Owner__c)){
                sr.OwnerId = UpdateOwnerResultMap.get(sr.AV_Study_Release_Owner__c).AV_User__c;
            } 
        }
    
    }
   /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       1. Updates owner that matched the "Study Release Owner"'s contact on Insert
   * @Paramters        List<AV_Study_Releases__c>:Trigger.New 
                       Map<Id, AV_Study_Releases__c>:Trigger.newMap
   * @return           void
   *********************************************************************************************************/
    public static void OnBeforeInsert (List<AV_Study_Releases__c> newValue, Map<Id, AV_Study_Releases__c> newMapValue)
    {
        Map<Id,Contact> contactMapUpdateOwner = new Map<Id,Contact>();
        Contact loggedInCon = AV_SOQLQueryUtility_StudyReleases.getContactOfLoggedInUser();
        Set<Id> listContactIds = new Set<Id>();
        for(AV_Study_Releases__c studyRelease : newValue){//list all contact Ids of study releases whose study release owner is change and status is Draft/In process            
            if(studyRelease.AV_Study_Release_Owner__c != Null && studyRelease.AV_Study_Release_Owner__c != loggedInCon.Id){
                listContactIds.add(studyRelease.AV_Study_Release_Owner__c);              
            }                   
        }
        if(!listContactIds.isEmpty()){
            contactMapUpdateOwner = AV_SOQLQueryUtility_StudyReleases.getContactForOwnerChange(listContactIds);
        }
       for(AV_Study_Releases__c sr : newValue){
            //Update Owner from Study Release Owner
            if(sr.AV_Study_Release_Owner__c != Null && sr.AV_Study_Release_Owner__c != loggedInCon.Id){
                sr.OwnerId = contactMapUpdateOwner.get(sr.AV_Study_Release_Owner__c).AV_User__c;
            }
            else{
                sr.AV_Study_Release_Owner__c = Id.valueOf(loggedInCon.Id);
            }
           //sr.OwnerId = sr.AV_Study_Release_Owner__c != Null ? contactMapUpdateOwner.get(sr.AV_Study_Release_Owner__c).AV_User__c :contactMapUpdateOwner.get(loggedInCon.Id).AV_User__c;
       } 
    }
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       1. Verifies the study release status is Draft/In Process
                       2. Calls method which copies the read only study fields
   * @Paramters        List<AV_Study_Releases__c>:Trigger.New
                       Map<Id, AV_Study_Releases__c>:Trigger.newMap
   * @return           void
   *********************************************************************************************************/
   
    public static void OnAfterInsert(List<AV_Study_Releases__c> newValue, Map<Id, AV_Study_Releases__c> newMapValue){
        Map<Id,RecordType> recordTypeMap = new Map<Id,RecordType>([SELECT Id,developerName,Name FROM RecordType where developerName IN ('AV_Interventional','AV_Observational')]);
        Map<Id,AV_Study__c> studyMapCopyFields = new Map<Id,AV_Study__c>();
        Set<Id> listStudyIds = new Set<Id>();
        for(AV_Study_Releases__c studyRelease : newValue){//list all contact Ids of study releases whose study release owner is change and status is Draft/In process            
            if((studyRelease.AV_Status__c == AV_StaticConstants.Draft || studyRelease.AV_Status__c == AV_StaticConstants.InProcess) 
                && studyRelease.AV_Protocol_Number__c != Null
                && (recordTypeMap.containsKey(studyRelease.RecordTypeId))){
                listStudyIds.add(studyRelease.AV_Protocol_Number__c);               
            }                      
        }
        if(!listStudyIds.isEmpty()){
            Map<Id,sObject> studyMap = new Map<Id,sObject>();
            Map<Id,sObject> studyReleaseMap = new Map<Id,sObject>();
            studyMap = createQueryToCopyFields('AV_Study__c',listStudyIds,'Id');
            studyReleaseMap = createQueryToCopyFields('AV_Study_Releases__c',newMapValue.keySet(),'Id');
            copyFieldsFromStudy(studyMap,studyReleaseMap);
        }    
    }
   /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       Update study release fields from its study
   * @Paramters        Map<Id,AV_Study__c> : Map of Study object from which values should be copies
                       Map<Id,AV_study_Releases__c>: Map of Study Release object needs to be update
   * @return           Void: None
   *********************************************************************************************************/
    public static void copyFieldsFromStudy(Map<Id,sObject> studyMap, Map<Id,sObject> studyReleaseMap){
        for(sObject studyrelease : studyReleaseMap.values())
        {
            AV_Study_Releases__c sr = (AV_Study_Releases__c)studyrelease;
            AV_Study__c study = (AV_study__c)studyMap.get(sr.AV_Protocol_Number__c);
            for(Schema.FieldSetMember f : getFields()) {
                if(f.getFieldPath() != AV_StaticConstants.protocolNumber)
                sr.put(f.getFieldPath(),study.get(f.getFieldPath()));
            }
        }
        try{
            Database.update(studyReleaseMap.values());
        }
        catch(Exception ex){
            AV_LOG_LogMessageUtility.logMessage(ex, 'AV_StudyReleaseHelperClass', AV_StaticConstants.EXC_TYPE_APP, 'CopyFieldsFromStudyToStudyRelease', 'Error', TRUE);    
        }  
    }
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       1. returns the Study Release FieldSet values 
   * @Paramters        None 
   * @return           List<Schema.FieldSetMember>: FieldSet members
   *********************************************************************************************************/
    public static List<Schema.FieldSetMember> getFields() {
        return SObjectType.AV_Study_Releases__c.FieldSets.Study_Fields.getFields();
    }
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       Forms the dynamic query from API name,Where clause parameters
   * @Paramters        Set<Id> : Set of Ids which used in Where cluse
                       ObjAPI: Object API name on which query should be form
                       whereClause: On which parameter where clause should form
   * @return           Map<Id,sObject>: Map of Study/Study Release objects
   *********************************************************************************************************/
   public static Map<Id,sObject> createQueryToCopyFields(String ObjAPI,Set<Id> objIds,String whereClause){
       String query = 'SELECT ';
            for(Schema.FieldSetMember f : getFields()) {
                If(f.getFieldPath() != AV_StaticConstants.protocolNumber)
                    query += f.getFieldPath() + ', ';
            }
            //Add Study lookup field if Query on Study Release
            if(ObjAPI == AV_StaticConstants.studyReleaseAPI){
                query += 'AV_Protocol_Number__c ,';
            }
            query += 'Id, Name FROM '+ObjAPI+' WHERE '+whereClause+' IN :objIds';
            //Add Where Clause to check study release status to Draft/InProcess
            if(ObjAPI == 'AV_Study_Releases__c'){
                query += ' AND (AV_Status__c = \''+AV_StaticConstants.Draft+'\' OR AV_Status__c = \''+AV_StaticConstants.InProcess+'\')';
            } 
            system.debug('query**'+query);
            Map<Id,sObject> objMap;
            try{
                objMap = new Map<Id,sObject>((List<sObject>)Database.query(query));
            }
            catch(Exception ex){
            }  
            system.debug('objMap **'+objMap);
            return objMap;
   }
}