/*********************************************************************************************************
 * Class Name   : AV_SOQLQueryUtility_StudyReleases 
 * Description  : Generic service class for SOQL queries on AV_Study_Releases__c
 * Created By   : Deloitte
 * Created On   : 11/11/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                11/11/2016              Initial version
*/
public with sharing class AV_SOQLQueryUtility_StudyReleases {
    
    public AV_SOQLQueryUtility_StudyReleases(){   
    }
    
    public static AV_Study_Releases__c getStudyReleaseDetail(String id){
        String query = 'Select Id,AV_Status__c,AV_Disclosure_Type__c from AV_Study_Releases__c where Id = :id';
        AV_Study_Releases__c studyReleaseData = Database.query(query);
        return studyReleaseData;
    }
    
     public static Map<Id,Contact> getContactForOwnerChange(Set<Id> listContactIds){
        Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id,AV_User__c FROM Contact WHERE Id IN :listContactIds]);
        return contactMap;
    }
    
    public static Contact getContactOfLoggedInUser(){
        Contact con = [SELECT Id,AV_user__c FROM Contact WHERE AV_User__c = :UserInfo.getUserId() Limit 1];
        return con;
    }
}