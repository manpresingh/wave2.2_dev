/*********************************************************************************************************
 * Class Name   : AV_SOQLQueryUtility_User 
 * Description  : Generic service class for SOQL queries on User object.
 * Created By   : Deloitte
 * Created On   : 29/09/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                29/09/2016                                   Initial version
*/
public with sharing class AV_SOQLQueryUtility_User {
    public AV_SOQLQueryUtility_User() {
        
    }
   /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-09-29
   * @description      Returns contact Id of the user
   * @Paramters        String: User ID
   * @return           contact Id
   *********************************************************************************************************/
   public static Id getUserContactId(String userId){
     list<User> userLst = [select Id, Name, AV_ContactID__c from User where ID=:userId limit 1];
     Id currentUserContactId = null;
     if(userLst!=null && !userLst.isEmpty() && userLst[0]!=null){
      currentUserContactId = userLst[0].AV_ContactID__c;
     }
     return currentUserContactId;
   }
   
   /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-17
   * @description      Returns contact Id of the user
   * @Paramters        String: User ID
   * @return           contact Id
   *********************************************************************************************************/
   public static list<user> UserContactID(String userId){
   
   list<User> users = [select Id, AV_ContactID__c from User where Id =:userId];
   return users;
   }
}