/*********************************************************************************************************
 * Class Name   : AV_SOQLQueryUtility_RecordType
 * Description  : Generic service class for SOQL queries on RecordType.
 * Created By   : Deloitte
 * Created On   : 11/11/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                11/11/2016                                   Initial version
*/

public with sharing class AV_SOQLQueryUtility_RecordType{

   /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-11
   * @description      gets recordTypeId using developer name
   * @Paramters        list<String> containing unique values of type values to be queried
   * @return           String containing recordtypeid
   *********************************************************************************************************/
   public static String getRecordTypeId(String objectName, String recordDeveloperName){
       //logic to get recordtypeid for monitoring record type in rule config object using its developer name
       String MonitoringRT = [Select Id From RecordType Where SobjectType = :objectName and DeveloperName = :recordDeveloperName].Id;
       return MonitoringRT;
       
   }
}