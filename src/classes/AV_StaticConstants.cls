/* Class Name   : AV_StaticConstants 
 * Description  : Generic class for Static Constants.
 * Created By   : Deloitte
 * Created On   : 04/06/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte        4/06/2016                                   Initial version
*/
global class AV_StaticConstants {
    
    //Event Number 5700
    public static final String Event_No_5700='Event_No_5700';
   
    
    
    //Event Number 5750
    public static final String Event_No_5750 ='Event_No_5750';

    //Event Number 5700
    public static final String Event_No_5800='Event_No_5800';
    
    //Event Number 5750
    public static final String Event_No_5850 ='Event_No_5850';
      
    //Event Number 8800
    public static final String  Event_No_8800 ='Event_No_8800';
    
    //Event Number 8850
    public static final String Event_No_8850 ='Event_No_8850';
    
    //Event Number 3700
    public static final String Event_No_3700 ='Event_No_3700';
    
    //Event Number 4900
    public static final String Event_No_4900 ='Event_No_4900';
    
     //Event Number 3300
    public static final String Event_No_3300 ='Event_No_3300';
    
     //Event Number 3200
    public static final String Event_No_3200 ='Event_No_3200';
    
    //Event Number 3500
    public static final String Event_No_3500 ='Event_No_3500';
    
    //Event Number 3600
    public static final String Event_No_3600 ='Event_No_3600';
    
    //Event Number 9700
    public static final String Event_No_9700 ='Event_No_9700'; 

    //Event Number 8784
    public static final String Event_No_8784 ='8784';
    
    //Original Protocol Ammendment No
    public static final String Original_Protocol_Ammendment_No ='0';

    // Exception Type Application
    public static final String EXC_TYPE_APP ='Application'; 
    // Colon
    public static final String CONSTANT_COLON =':';
    // Comma
    public static final String CONSTANT_COMMA =',';    
    //Boolean to avoide recursive execution in trigger 
    public static Boolean TRIGGER_IS_RECURSIVE = FALSE;

    //
    public static String STUDY_NAME = 'StudyName'; 

    public static String EVENT = 'Event';  

    public static String STUDYID  ='StudyID';

    // Protocol Approval Object Name
    public static final String PA_APPROVAL_OBJ_NAME = 'AV_Protocol_Approval_and_Amendment__c';
    
    // Protocol Ammendment Object Name
    public static final String PA_OBJ_NAME = 'AV_Protocol_Amendment__c';

    // Company Personnel (Contact) Object Name
    public static final String CP_OBJ_NAME = 'Contact';

    // External ID Date Format
    public static final String EXTERNALID_DATE_FORMAT = 'MM/dd/yyyyHH:mm:ss';
    
    //Electra Study Releases Status Field Value
    public static final String Approved='Approved';
    public static final String Generated_XML='Generated XML';
    public static final String Draft = 'Draft';
    public static final String InProcess = 'In Process';
    //Electra 
    public static final String studyAPI = 'AV_Study__c';
    public static final String studyReleaseAPI = 'AV_Study_Releases__c';
    public static final String protocolNumber = 'AV_Protocol_Number__c';

    //Rule Configuration Object Name
    public static final String RULECONFIG_OBJ_NAME = 'AV_Rule_Configuration__c';
    
    //Rule Configuration Recordtype
    public static final String RULECONFIG_MONITORING_RTYPE_NAME = 'AV_Monitoring_Activity_Rule_Configuration';

    //Rule Configuration Status values
    public static final String STATUS_ACTIVE = 'Active';
    public static final String STATUS_INACTIVE = 'Inactive';
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_INPROCESS = 'In Process';

    //Checklist Question object field name 
    public static final String CHECKLIST_QUESTION_STATUS_API_NAME = 'av_checklist_question_status__c';

    //Standard fields Last modified date and last modified by
    public static final String LASTMODIFIEDDATE_API_NAME = 'lastmodifieddate';
    public static final String LASTMODIFIEDBYID_API_NAME = 'lastmodifiedbyid';
    
    //Study Release Status values
    public static final String STATUS_RELEASED = 'Released';
    public static final String STATUS_COMPLETED = 'Completed';
    public static final String STATUS_CANCELLED = 'Cancelled';
    
    //Study Release Disclosure Type
    public static final String OTHER_PUBLIC_DISCLOSURE_TYPE = 'Other Public Disclosure';

    //Checklist Question object Name
    public static final String CHECKLIST_QUESTION_OBJ_NAME = 'AV_Checklist_Questions__c';

    //record type id storage for AV_Monitoring_Activity_Rule_Configuration developer name in AV_Rule_Configuration__c object
    public static String monActivityRuleConfigRTid = '';
}