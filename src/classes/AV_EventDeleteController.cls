/********************************************************************************************************
* @author         Deloitte
* @description    AV_EventDeleteController used to perform Event Soft delete, 
                  performing hard delete on related Event Dates.
                  We are not performing soft delete for system events. 
 
* @date           2016-03-08
*********************************************************************************************************

 * Modification Log:  
 * ------------------------------------------------------------------------------------------------------
 * Developer                Date            Modification ID             Description 
 * ------------------------------------------------------------------------------------------------------
 * Sai kalyan             2016-03-08                                    Initial version
*/

public with sharing class AV_EventDeleteController{


    public AV_Event__c masterEvent;
    
    public String warningMessage {
        get;
        set;
    }
    
    public Static String currentClassName;
    public boolean showError{get;set;}
    
    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-08 
    * @description   Constructor used to get Class Name 
    * @Paramters     none
    * @return        none
    *********************************************************************************************************/  

    public AV_EventDeleteController(ApexPages.StandardController stdController)
    {
        /*list<AV_Event__c> lstEvent= new list<AV_Event__c>([Select ID from AV_Event__c]);
           list <AV_Rule_for_Event_Assignment__c> lstREA = new list<AV_Rule_for_Event_Assignment__c>([SELECT AV_Event_Description__c FROM AV_Rule_for_Event_Assignment__c]);
           map<ID,AV_Rule_for_Event_Assignment__c> mapREA = new map<ID,AV_Rule_for_Event_Assignment__c>();
                for(AV_Rule_for_Event_Assignment__c objREA: lstREA){
                        mapREA.put(objREA.AV_Event_Description__c , objREA);
    }
    for( AV_Event__c en: lstEvent){
            if(mapREA.containskey(en.ID)){
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Cannot Delete'));
                }
         }*/
        masterEvent = (AV_Event__c) stdController.getRecord();
        showError=false;
       
        String objectVar = String.valueOf(this);
        if(String.isNotBlank(objectVar) && objectVar.contains(AV_StaticConstants.CONSTANT_COLON))
        {
            currentClassName = objectVar.substring(0,objectVar.indexOf(AV_StaticConstants.CONSTANT_COLON));
        }   
    }
    

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-08 
    * @description   checkforDelete Method Used to Check if event can be deleted.
    *                 1) System Event cannot be deleted.
    *                 2) User event cannot be deleted if it has one associated Event dates record with actual or planned Date 


    * @Paramters     none
    * @return        none
    *********************************************************************************************************/ 

    public pagereference softDeleteUserEvent(){
           /*list<AV_Event__c> lstEvent= new list<AV_Event__c>([Select ID from AV_Event__c]);
           list <AV_Rule_for_Event_Assignment__c> lstREA = new list<AV_Rule_for_Event_Assignment__c>([SELECT AV_Event_Description__c FROM AV_Rule_for_Event_Assignment__c]);
           map<ID,AV_Rule_for_Event_Assignment__c> mapREA = new map<ID,AV_Rule_for_Event_Assignment__c>();
                for(AV_Rule_for_Event_Assignment__c objREA: lstREA){
                        mapREA.put(objREA.AV_Event_Description__c , objREA);
    }
    for( AV_Event__c en: lstEvent){
            if(mapREA.containskey(en.ID)){
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Cannot Delete'));
                }
         }*/
         list <AV_Rule_for_Event_Assignment__c> ruleList=[Select id from AV_Rule_for_Event_Assignment__c where AV_Event_Description__c=:masterEvent.id];
            if(ruleList.size()>0){
                system.debug('I am here');
                showError=true;
                //masterEvent.addError(' You are not allowed to delete this record');
                system.debug('I am here'+showError);
                //system.debug('Add Errr'+masterEvent.addError(' You are not allowed to delete this record'));
                //system.debug('Error Message'+ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Cannot Delete')));
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Cannot delete events assigned to any Rule for Event Assignment'));
                
                
        }
        
            PageReference pageReference = null;
            if(!showError){
            String systemRecordTypeId = AV_Common_Utilities.fetchRecordTypeID('AV_Event__c',Label.AV_System_Event);
            try{
                if(!systemRecordTypeId.equalsIgnoreCase(masterEvent.RecordTypeId)){
                    List<AV_Event_Date__c> eventDateRecords = [SELECT Id, 
                                                            AV_Planned_Date__c, 
                                                            AV_Actual_Date__c, 
                                                            AV_Event__c FROM 
                                                            AV_Event_Date__c WHERE AV_Event__c =: masterEvent.Id];     //This query used to get event dates records
                    
                    if(eventDateRecords != null && !eventDateRecords.isEmpty()) {
    
                        for(AV_Event_Date__c eachEventDate : eventDateRecords) {
                          if(eachEventDate.AV_Actual_Date__c != null || eachEventDate.AV_Planned_Date__c != null) {
                                warningMessage = Label.AV_EventDelete;                   //This message will display when event date associated with actual & planned date
                                break;
                            }
                        }
                        if(String.isBlank(warningMessage)) {
                            try{
    
                                if(masterEvent.ID !=null) {
                                    masterEvent.OwnerId= Label.AV_SoftDeleteOwnerId;
                                    masterEvent.AV_Is_Soft_Delete_Apex__c = true;
                                    masterEvent.AV_Is_Batch_Executed_Apex__c = true;
                                    boolean isListNotEmpty = false;
                                    if(masterEvent !=null){
                                        update masterEvent;          //Performing soft delete on events
                                    }
    
                                    if(eventDateRecords !=null && !eventDateRecords.isEmpty()){ 
                                        if(eventDateRecords.Size() < 3000){
                                            Delete eventDateRecords; //Performing hard delete on related eventdates   
                                        }else{
                                            Database.executeBatch(new AV_EventDeleteBatch(), Limits.getLimitDMLRows()-100);    
                                        }  
                                    }
                                    pageReference = fetchPagereference();
                                }
                            }catch (Exception saveException){
    
                                AV_LOG_LogMessageUtility.logMessage(saveException, currentClassName, AV_StaticConstants.EXC_TYPE_APP, 'softDeleteUserEvent', '', TRUE);
                                System.debug(LoggingLevel.ERROR, 'Exception while fetching record :' + saveException);
                            }
                        } 
                    }else{
                            masterEvent.OwnerId= Label.AV_SoftDeleteOwnerId;
                            masterEvent.AV_Is_Soft_Delete_Apex__c = true;
                            masterEvent.AV_Is_Batch_Executed_Apex__c = true;
                            try{
                                if(masterEvent!=null){
                                    if(masterEvent!=null){
                                        update masterEvent;
                                    }
                                    pageReference = fetchPagereference();
                                }
                            }catch (Exception commonException) {
                                AV_LOG_LogMessageUtility.logMessage(commonException, currentClassName, AV_StaticConstants.EXC_TYPE_APP, 'softDeleteUserEvent', '', TRUE);
                                System.debug(LoggingLevel.ERROR, 'Exception while fetching record :' + commonException);
                            }
                    }
                    
                }else{
                    warningMessage = Label.AV_Event_Update_ErrorMessages; 
                }
            }catch (Exception exceptionVar){
                AV_LOG_LogMessageUtility.logMessage(exceptionVar, currentClassName, AV_StaticConstants.EXC_TYPE_APP, 'CheckforDelete', '', TRUE);
                System.debug(LoggingLevel.ERROR, 'Exception while fetching record :' + exceptionVar);
            }
         }
        return pageReference;
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          2016-03-08 
    * @description   fetchPagereference Method Used to redirect the Page 
    * @Paramters     none
    * @return        none
    *********************************************************************************************************/
   
    private Pagereference fetchPagereference(){
        Schema.DescribeSObjectResult SObjectSchema = AV_Event__c.SObjectType.getDescribe();
        String objectIdPrefix = SObjectSchema.getKeyPrefix();
        PageReference pageReference = new PageReference('/'+objectIdPrefix+'/o');
        pageReference.setRedirect(true);
        return pageReference;
    }
}