public class AV_CopyStudyToStudyReleaseClass
{
    public AV_CopyStudyToStudyReleaseClass(){}
    
    public Map<Id,String> copyFromStudyrelatedlist(Set<Id> studyIds,string objAPI){
        String queryString = 'SELECT Id,Name,AV_Protocol_Number__c FROM '+objAPI+' WHERE AV_Protocol_Number__c IN :studyIds';
        List<sObject> studyrelatedList = Database.Query(queryString);
        String str = '';
        Map<Id,String> studyThetapeuticMap = new Map<Id,String>();
        for(sObject thera : studyrelatedList){
            if(studyThetapeuticMap.containsKey(String.valueOf(thera.get('AV_Protocol_Number__c')))){
                String s = studyThetapeuticMap.get(String.valueOf(thera.get('AV_Protocol_Number__c')));
                s += thera.get('Name')+',';
                studyThetapeuticMap.put(String.valueOf(thera.get('AV_Protocol_Number__c')),s);
            }
            else{
                studyThetapeuticMap.put((Id)thera.get('AV_Protocol_Number__c'),thera.get('Name')+',');
            }         
        }
        return studyThetapeuticMap;
    }
    
    
}