/* 
 * Class Name   : AV_StudyTriggerHelper 
 * Description  : AV_StudyTriggerHelper Used for AV_Study Triggers
 * Created By   : Deloitte
 * Created On   : 03/11/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte            03/11/2016                                    Initial version
 */
 
public class AV_StudyTriggerHelper {

    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-06-10
   * @description      This method is used to update the Study Site and Study Country Name field whenever its parent study Protocol number is changed.
   * @Paramters        Map<id, AV_Study__c>: Map of new Study records; Map<id, AV_Study__c>: Map of old study records
   * @return           void
   *********************************************************************************************************/
    public static void updateCountrySiteName(Map<id, AV_Study__c> mapOldStudyRecords, Map<id, AV_Study__c> mapNewStudyRecords){
        List<String> lstOfStudyId = new List<String>();

        for(AV_Study__c eachProtocalstudy :mapNewStudyRecords.values()){
            if(eachProtocalstudy.Name != mapOldStudyRecords.get(eachProtocalstudy.id).Name ){
                // Store the STidy Ids if Study name changes from old record to new record
                lstOfStudyId.add(eachProtocalstudy.id);
            }    
        }
        if(!lstOfStudyId.isEmpty()){    // If such conatcts exists
            List<AV_Study_Country__c> lstStudyCountry = new List<AV_Study_Country__c>();//Variable is used to save the list of Study Country which needs to be updated
            List<AV_Study_Site__c> lstOfStudySite = new List<AV_Study_Site__c>();//Variable is used to save the list of Study Site which needs to be updated
            
            // Fetch the Study countries related to all teh above studies
            lstStudyCountry = [Select id, Name, AV_Study__c, AV_Country_Name__c,(Select id, AV_Principal_Investigator__r.LastName, AV_Principal_Investigator__r.FirstName, AV_Primary_Facility__r.Name, Name From Study_Sites__r) from AV_Study_Country__c Where AV_Study__c IN: lstOfStudyId];
            
            if(lstStudyCountry != null && lstStudyCountry.size()>0){
                for(AV_Study_Country__c eachStudyCountry : lstStudyCountry){
                    eachStudyCountry.Name = mapNewStudyRecords.get(eachStudyCountry.AV_Study__c).Name+' - '+eachStudyCountry.AV_Country_Name__c;
                    if(eachStudyCountry.Study_Sites__r != null && eachStudyCountry.Study_Sites__r.size()>0){
                        for(AV_Study_Site__c eachStudySite : eachStudyCountry.Study_Sites__r){
                            // Form the Study site name (country name + principal investigatr + Primary facility)
                            eachStudySite.Name = eachStudyCountry.Name+' - '+eachStudySite.AV_Principal_Investigator__r.FirstName+','+eachStudySite.AV_Principal_Investigator__r.LastName+' - '+eachStudySite.AV_Primary_Facility__r.Name;
                            lstOfStudySite.add(eachStudySite);
                        }
                    }
                }
                try{
                    // Update Study Countries and Study Sites
                    update lstStudyCountry;
                    if(lstOfStudySite != null && lstOfStudySite.size()>0){
                        update lstOfStudySite;
                    }
                }catch(Exception commonException){
                    AV_LOG_LogMessageUtility.logMessage(commonException, 'AV_StudyTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'updateCountrySiteName', 'Error', TRUE);
                }
            }
        }
    }
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-14
   * @description      Method performs below operations:
                       1. updates the study release fields that matches the study fields on update
   * @Paramters        Map<Id,AV_Study__c> : Trigger.New
                       Map<Id,AV_Study__c>: Trigger.newMap
                       Map<Id,AV_Study__c>: Trigger.oldMap
   * @return           Void: None
   *********************************************************************************************************/
    public static void copyFieldsToStudyRelease(List<AV_Study__c> newValueList,Map<Id,AV_Study__c> newValueMap,Map<Id,AV_Study__c>oldValueMap){
        Map<Id,AV_Study__c> StudyObjMap = new Map<Id,AV_Study__c>();
        for(AV_Study__c studyObj : newValueList){//Verify that 
            AV_Study__c oldStudyObj = oldValueMap.get(studyObj.Id);
            for(Schema.FieldSetMember f : AV_StudyReleaseHelperClass.getFields()) {
                if(f.getFieldPath() != AV_StaticConstants.protocolNumber)
                if(studyObj.get(f.getFieldPath()) != oldStudyObj.get(f.getFieldPath()))
                {
                     StudyObjMap.put(studyObj.Id,studyObj);   
                }
            }
        }
        Map<Id,sObject> studyReleaseMap = new Map<Id,sObject>();
        studyReleaseMap = AV_StudyReleaseHelperClass.createQueryToCopyFields('AV_study_Releases__c',StudyObjMap.keySet(),'AV_Protocol_Number__c');
        AV_StudyReleaseHelperClass.copyFieldsFromStudy(StudyObjMap,studyReleaseMap);       
    }
}