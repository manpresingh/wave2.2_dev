/********************************************************************************************************
* @author         Deloitte
* @description    This is a test class for the trigger on AV_Checklist_Questions__c object
* @date           11/10/2016
* 
* Modification Log:
* -------------------------------------------------------------------------------------------------------
* Developer                Date                   Modification ID      Description
* -------------------------------------------------------------------------------------------------------
* Deloitte                 11/10/2016                                   Initial version
*********************************************************************************************************/
@isTest(seeAllData=false)
private class AV_Test_ChecklistQuestionTriggerHelper {

    private static AV_Checklist_Questions__c cQuestion = new AV_Checklist_Questions__c();
    private static AV_Rule_Configuration__c rConfig = new AV_Rule_Configuration__c();
    private static AV_Rule_for_Checklist__c rule4Check = new AV_Rule_for_Checklist__c();
    private static User businessAdmin = new User();

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/10/2016
    * @description   initialize data
    * @param         
    * @return        
    *********************************************************************************************************/
    static {
        User sysadmin = AV_LOG_TestDataUtility.createTestUser('System Administrator', 'admin', null);
        System.runAs(sysadmin) {
            businessAdmin = AV_LOG_TestDataUtility.createTestUser('Tesla Business Administrator', 'bizadmin', 'Tesla_System_Admin');
            cQuestion = AV_LOG_TestDataUtility.createChecklistQuestion(AV_StaticConstants.STATUS_ACTIVE, 'Sample Question', 'Sample Topic', 'Yes;No');
            insert cQuestion;        
        }
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/10/2016
    * @description   create test data
    * @param         
    * @return        
    *********************************************************************************************************/
    static void createData() {
        rConfig = AV_LOG_TestDataUtility.createMonitoringRuleConfiguration('Safety Review', AV_StaticConstants.STATUS_DRAFT);
        insert rConfig;
        System.debug('Check:rConfig = '+rConfig);
        rule4Check = AV_LOG_TestDataUtility.createRuleForChecklist(rConfig.Id, cQuestion.Id, 1);
        insert rule4Check;
        rConfig.AV_Status__c = AV_StaticConstants.STATUS_ACTIVE;
        update rConfig;
        
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   This method tests validation error when data fields are edited on an Active  Question record.
    * @param         
    * @return        
    *********************************************************************************************************/
    public static testmethod void testEditActiveQuestion() {
        String resultError;
        System.runAs(businessAdmin) {
            test.StartTest();
                try{
                    cQuestion.AV_Question__c = 'Updated Sample Question';
                    update cQuestion;
                }
                catch(Exception exp) {
                    resultError = exp.getMessage();
                }
            test.StopTest();
        }
        System.assert(resultError.contains(System.Label.AV_Update_On_Active_Inactive_Question_Error));
    }

    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/11/2016
    * @description   This method tests the error when the Status is updated on a Question with active Rule
    *                Configuration record associated to it.
    * @param         
    * @return        
    *********************************************************************************************************/
    public static testmethod void testEditActiveRuleQuestion() {
        String resultError;
        System.runAs(businessAdmin) {
            createData();
            test.StartTest();
                try{
                    cQuestion.AV_Checklist_Question_Status__c = AV_StaticConstants.STATUS_INACTIVE;
                    update cQuestion;
                }
                catch(Exception exp) {
                    resultError = exp.getMessage();
                }
            test.StopTest();
        }
        System.assert(resultError.contains(System.Label.AV_Status_Change_Error_Message));
    }

}