/* 
 * Class Name   : AV_Rule_Event_AssignmentTriggerHelper
 * Description  : AV_Rule_Event_AssignmentTriggerHelper Used for AV_Rule_for_Event_AssignmentTrigger
 * Created By   : Deloitte
 * Created On   : 13/11/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                 Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte            05/06/2016                                    Initial version
 */
 
public class AV_Rule_Event_AssignmentTriggerHelper{

    /********************************************************************************************************
   * @author           Deloitte
   * @date             13/11/2016
   * @description      
   * @Paramters        List<AV_Rule_for_Event_Assignment__c>: List of new Rule for event assignment;
   * @return           void
   *********************************************************************************************************/
    public static void CheckMultileStyudySetEvent(List<AV_Rule_for_Event_Assignment__c> lstTriggerNew){
    map<Id, Id> mapAllEvent = new map<Id, Id>();
    Map<id, List<AV_Rule_for_Event_Assignment__c>> mapidToRuleForEventAssignment = new Map<id, List<AV_Rule_for_Event_Assignment__c>>();
    try{
    for(AV_Rule_for_Event_Assignment__c eachEventAssign : lstTriggerNew){
    mapAllEvent.put(eachEventAssign.AV_Event_Description__c, eachEventAssign.id);
    }
    for(AV_Event__c  eachEvent : [Select id, (select id, AV_Study_Rule_Set__c, AV_Event_Description__c from Rule_for_Event_Assignments__r) from AV_Event__c where id IN: mapAllEvent.keyset()]){
    mapidToRuleForEventAssignment.put(mapAllEvent.get(eachEvent.id), eachEvent.Rule_for_Event_Assignments__r);
    }
    for(AV_Rule_for_Event_Assignment__c eachRuleSuper : lstTriggerNew){
    Integer count = 0;
    for(AV_Rule_for_Event_Assignment__c eachRuleSub : mapidToRuleForEventAssignment.get(eachRuleSuper.id)){
    if(eachRuleSuper.AV_Event_Description__c == eachRuleSub.AV_Event_Description__c && eachRuleSuper.AV_Study_Rule_Set__c == eachRuleSub.AV_Study_Rule_Set__c){
     count++;
    }
   }
   if(count > 1)
   eachRuleSuper.adderror('Same event cannot be added multiple times as rule for event assignment in same study rule set');
   }
   }catch(Exception errorMessage){

            //logging errors
            AV_LOG_LogMessageUtility.logMessage(errorMessage, 'AV_Rule_Event_AssignmentTriggerHelper', AV_StaticConstants.EXC_TYPE_APP, 'CheckMultileStyudySetEvent', 'Error', TRUE); 
        }
   }
   }