/********************************************************************************************************
* @author         Deloitte
* @description    Test Class for AV_Test_EnrollmentRuleTriggerHelper

* @date           11/9/2016
*********************************************************************************************************

 * Modification Log:  
 * ------------------------------------------------------------------------------------------------------
 * Developer                Date            Modification ID             Description 
 * ------------------------------------------------------------------------------------------------------
 * Deloitte                11/9/2016                                    Initial version
*/
@isTest(seeAllData=false)
public class AV_Test_EnrollmentRuleTriggerHelper{
/********************************************************************************************************
    * @author        Deloitte
    * @date          11/9/2016
    * @description   used to Create Test Data  
    * @Paramters     none
    * @return        none
    *********************************************************************************************************/
      static testmethod void testDuplicateRule(){
          Boolean Result=false;
          AV_Enrollment_Type__c Enroll= new AV_Enrollment_Type__c (AV_Enrollment_Type__c='Test Enrollment',AV_Enrollment_Type_Number__c='119', AV_Level__c='Study');
          insert Enroll;
          AV_Rule_Configuration__c Rule= new AV_Rule_Configuration__c(AV_Status__c='Draft');
          insert Rule;
          AV_Rule_for_Enrollment_Assignment__c  Rule1= new AV_Rule_for_Enrollment_Assignment__c (AV_Enrollment_Type__c=Enroll.ID,AV_Study_Rule_Set__c=Rule.ID);
          
          Test.startTest();
          insert Rule1;
         
          try{
              AV_Rule_for_Enrollment_Assignment__c  Rule2= new AV_Rule_for_Enrollment_Assignment__c (AV_Enrollment_Type__c=Enroll.ID,AV_Study_Rule_Set__c=Rule.ID);
               insert Rule2;
          }
          catch(DmlException ex){ result = true;}
          System.assert(result);
          Test.stopTest();
      }
}