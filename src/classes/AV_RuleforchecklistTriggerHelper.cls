/*********************************************************************************************************
 * Class Name   : AV_RuleforchecklistTriggerHelper
 * Description  : Ruleforchecklist Trigger Helper .
 * Created By   : Deloitte
 * Created On   : 11/11/2016

 * Modification Log:
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                11/11/2016                                   Initial version
*/
public class AV_RuleforchecklistTriggerHelper {
    
    /********************************************************************************************************
   * @author           Deloitte
   * @date             2016-11-11
   * @description      Performs validation of sequence number 
   * @Paramters        List<AV_Rule_for_Checklist__c> lstNewvalues (Trigger.new)
   * @return           void
   *********************************************************************************************************/
    public static void performSequenceValidation(List<AV_Rule_for_Checklist__c> lstNewvalues){
        
           List<AV_Rule_for_Checklist__c> lst_ChecklistResponse=AV_SOQLQueryUtility_RuleForChecklist.getSequenceNumber(); 
        for(AV_Rule_for_Checklist__c obj_checklistResponse: lstNewvalues){
            for(Integer i=0;i<lst_ChecklistResponse.size();i++){
                if(lst_ChecklistResponse.get(i).AV_Rule_Configuration__c== obj_checklistResponse.AV_Rule_Configuration__c){
                    if(lst_ChecklistResponse.get(i).AV_Sequence__c == obj_checklistResponse.AV_Sequence__c){
                        
                        obj_checklistResponse.adderror(Label.AV_sequence_error_checkresponselist);
                        
                     }
                }
            }
        } 
    } 

}