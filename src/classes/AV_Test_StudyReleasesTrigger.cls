@isTest

Private class AV_Test_StudyReleasesTrigger {
    
    static testmethod void ValidationTest(){
       
        Contact con = new Contact(LastName = 'Test');
        insert con;
        List<AV_Study_Releases__c> sr = new List<AV_Study_Releases__c>{new AV_Study_Releases__c(AV_Disclosure_Type__c='CT.gov',AV_Study_Release_Owner__c=con.Id ,AV_Status__c='Approved')};
        
        insert sr;
        
        List<AV_Study_Releases__c> stdr = new List<AV_Study_Releases__c>{[SELECT id from AV_Study_Releases__c WHERE ID IN : sr ]};
 
            for (AV_Study_Releases__c stdy : stdr){
                stdy.AV_Disclosure_Type__c = 'EudraCT';
                stdr.add(stdy);
            }
        Update stdr;
        
    } 
    
    static testmethod void verifyOwnerChange(){
        //Create User Test Data
        List<User> userList = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u1 = new User(Alias = 'standt', Email='sysadminuser1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='sysadminuser1@testorg.com');
        userList.add(u1);
        User u2 = new User(Alias = 'standt', Email='sysadminuser2@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='sysadminuser2@testorg.com');
        userList.add(u2);
        insert userList;
        //Create Contact Test Data
        List<Contact> contactList = new List<Contact>{new Contact(LastName='Roy',FirstName='John',AV_User__c=u1.Id,AV_Active_Inactive__c='Active'),new Contact(LastName='Min',FirstName='Max',AV_User__c=u2.Id,AV_Active_Inactive__c='Active')};
        insert contactList;
        //Create Study Test Data
        List<AV_Study__c> studydata = (List<AV_Study__c>)AV_TestDataCreation.createTestData('AV_Study__c',2,false);
        studydata[0].AV_CT_gov_Number__c = '1000';
        studydata[1].AV_Study_Status__c = 'Approved';
        insert studydata;
        //Create Study Release Test Data
        List<AV_Study_Releases__c> sr = new List<AV_Study_Releases__c>{new AV_Study_Releases__c(AV_Disclosure_Type__c='CT.gov',AV_Study_Release_Owner__c=contactList[0].Id ,AV_Status__c='In Process',AV_Protocol_Number__c = studydata[0].Id),new AV_Study_Releases__c(AV_Disclosure_Type__c='CT.gov',AV_Study_Release_Owner__c=contactList[1].Id ,AV_Status__c='Draft',AV_Protocol_Number__c = studydata[0].Id)};
        insert sr;
        
        //Verify that Owner is updated with Study Release Owner's User
        List<AV_Study_Releases__c> srList = [SELECT Id,OwnerId,Owner.Name,AV_Study_Release_Owner__r.Name,AV_CT_gov_Number__c FROM AV_Study_Releases__c WHERE Id = :sr[0].Id];
        sr[0].AV_Study_Release_Owner__c = contactList[1].Id;
        update sr;
        //Verify that Study fields are copied to Study Release
        system.assertEquals('1000',srList[0].AV_CT_gov_Number__c);
        AV_Study_Releases__c srList1 = [SELECT Id,OwnerId,Owner.Name,AV_Study_Release_Owner__r.Name,AV_Study_Status__c FROM AV_Study_Releases__c WHERE Id = :sr[0].Id];
        system.debug('srList**'+srList1);
        system.assertEquals(u2.Id,srList1.ownerId);
        //Verify that Study fields are copied to Study Release
        //system.assertEquals('Approved',srList1.AV_Study_Status__c);
    }

}