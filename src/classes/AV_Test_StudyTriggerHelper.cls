/* 
 * Class Name   : AV_StudyTriggerHelper
 * Description  : Test Class for AV_EventTriggerHelper
 * Created By   : Deloitte
 * Created On   : 05/10/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte               05/10/2016                                  Initial version
   
*/
@istest(seealldata=false)
public class AV_Test_StudyTriggerHelper{
   
   public static User loggedInUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
   public static list<AV_Study__c> studydata = AV_TestDataCreation.createTestData('AV_Study__c',5,true); 
   public static list<AV_Study_Country__c> studycountry=AV_TestDataCreation.createTestData('AV_Study_Country__c',5,true);
   public static list<contact> testcp=AV_TestDataCreation.createTestData('contact',5,false);
   public static list<AV_Center__c> centerlst=AV_TestDataCreation.createTestData('AV_Center__c',5,true);
   public static list<AV_Study_Site__c> Studysitedata = AV_TestDataCreation.createTestData('AV_Study_Site__c',5,true);
  // public static list<AV_Event__c> EvenAll=AV_TestDataCreation.createTestData('AV_Event__c',5,true);
  // public static list<AV_Event_Date__c> Evendate = AV_TestDataCreation.createTestData('AV_Event_Date__c',5,true);
   public static Map<Id,AV_Study__c> oldMap = new Map<Id,AV_Study__c>();
   public static Map<Id,AV_Study__c> newmap = new Map<Id,AV_Study__c>();
   
   
    //test method for checking the positive test case functionality
    public static testmethod void StudyTriggerHelperpositivetest() 
    {   
            system.runAs(loggedInUser){
            testcp[0].recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Clinical Personnel').getRecordTypeId();
            if(testcp!=null && !testcp.isEmpty()) 
            insert testcp;
                        
            oldmap.put(studydata[0].id,studydata[1]);
            newmap.put(studydata[0].id,studydata[0]);
            
            test.starttest();
            AV_StudyTriggerHelper.updateCountrySiteName(oldmap,newmap);
            //AV_StudyTriggerHelper.createEventDatesForStudy(newmap);
            system.assert(studycountry[0].id!=null);
            //system.assert(Evendate[0].id!=null);
            test.stoptest();
            }
    }
    
    //test method for checking the bulk test case functionality
     public static testmethod void StudyTriggerHelperbulktest() 
    {
        system.runAs(loggedInUser){
            
            list<AV_Study__c> studydata1 = AV_TestDataCreation.createTestData('AV_Study__c',150,true); 
            list<AV_Study_Country__c> studycountry1=AV_TestDataCreation.createTestData('AV_Study_Country__c',150,true);
            list<contact> testcp1=AV_TestDataCreation.createTestData('contact',5,false);
            for(Integer i=0; i<testcp1.size(); i++){
            testcp1[i].recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Clinical Personnel').getRecordTypeId();
            }
            if(testcp1!=null && !testcp1.isEmpty()) {
                   upsert testcp1;
            }
            
            list<AV_Center__c> centerlst1=AV_TestDataCreation.createTestData('AV_Center__c',1,true);
                    
            
            //for updating study site
            list<AV_Study_Site__c> Studysitedata1 = AV_TestDataCreation.createTestData('AV_Study_Site__c',150,true);
            
            
            
            //for updating event object test data
            //list<AV_Event__c> EvenAll1=AV_TestDataCreation.createTestData('AV_Event__c',150,true);
                        
            //for updating event date object test data
           // list<AV_Event_Date__c> Evendate1 = AV_TestDataCreation.createTestData('AV_Event_Date__c',150,true);
            
            Integer i=0;
            for(AV_Study__c study:studydata1){
            
            oldmap.put(study.id,study);
            newmap.put(study.id,study);
            }
            test.starttest();
            AV_StudyTriggerHelper.updateCountrySiteName(oldmap,newmap);
           // AV_StudyTriggerHelper.createEventDatesForStudy(newmap);
            system.assert(studycountry[0].id!=null);
            //system.assert(Evendate[0].id!=null);
            test.stoptest();
            
        }
        
        
    }
    
    //test method for checking the different profile case functionality
    public static testmethod void StudyTriggerdiffprofiletest() 
    {
     User BAadmin = [SELECT Id FROM User WHERE ProfileID =:Label.AV_BusinessAdmin_ID AND ISActive = TRUE limit 1];
     system.runAs(BAadmin){
      testcp[0].recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Clinical Personnel').getRecordTypeId();
            if(testcp!=null && !testcp.isEmpty()) 
            insert testcp;
                        
            oldmap.put(studydata[0].id,studydata[1]);
            newmap.put(studydata[0].id,studydata[0]);
            
            test.starttest();
            AV_StudyTriggerHelper.updateCountrySiteName(oldmap,newmap);
            //AV_StudyTriggerHelper.createEventDatesForStudy(newmap);
            system.assert(studycountry[0].id!=null);
            //system.assert(Evendate[0].id!=null);
            test.stoptest();
     
        }
    
    }
      
    //test method for checking the negative case functionality
    public static testmethod void StudyTriggernegativetest() 
    {
        system.runAs(loggedInUser){
            
            //Evendate[0].id=null;
            
            testcp[0].recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Clinical Personnel').getRecordTypeId();
            if(testcp!=null && !testcp.isEmpty()) 
            insert testcp;
                        
            oldmap.put(studydata[0].id,studydata[1]);
            newmap.put(studydata[0].id,studydata[0]);
            
            test.starttest();
            AV_StudyTriggerHelper.updateCountrySiteName(oldmap,newmap);
           // AV_StudyTriggerHelper.createEventDatesForStudy(newmap);
            system.assert(studycountry[0].id!=null);
            //system.assert(Evendate[0].id==null);
            test.stoptest();
        }
        
    }
    
    public static testmethod void testStudyToStudyRelesemapping(){
    //Create Contact Test Data
        List<Contact> contactList = new List<Contact>{new Contact(LastName='Roy',FirstName='John',AV_User__c=UserInfo.getUserId(),AV_Active_Inactive__c='Active'),new Contact(LastName='Min',FirstName='Max',AV_User__c=UserInfo.getUserId(),AV_Active_Inactive__c='Active')};
        insert contactList;
        //Create Study Release Test Data
        List<AV_Study_Releases__c> sr = new List<AV_Study_Releases__c>{new AV_Study_Releases__c(AV_Disclosure_Type__c='CT.gov',AV_Study_Release_Owner__c=contactList[0].Id ,AV_Status__c='In Process',AV_Protocol_Number__c = studydata[0].Id),new AV_Study_Releases__c(AV_Disclosure_Type__c='CT.gov',AV_Study_Release_Owner__c=contactList[1].Id ,AV_Status__c='Draft',AV_Protocol_Number__c = studydata[0].Id)};
        insert sr;
        studydata[0].AV_CT_gov_Number__c = '1000';
        studydata[0].AV_Study_Status__c = 'Approved';
        update studydata[0];
        //Verify that Study Release fields are updated
        List<AV_Study_Releases__c> srList = [SELECT Id,OwnerId,Owner.Name,AV_Study_Release_Owner__r.Name,AV_CT_gov_Number__c,AV_Study_Status__c FROM AV_Study_Releases__c WHERE Id = :sr];    
        system.debug('srList***'+srList);
        system.assertEquals(studydata[0].AV_CT_gov_Number__c,srList[0].AV_CT_gov_Number__c);
        system.assertEquals(studydata[0].AV_Study_Status__c,srList[0].AV_Study_Status__c);
    }

      
            
}