/* 
 * Apex Class Name : AV_EnrollmentRuleTriggerHelper
 * Description     : This Class used as a helper class to AV_EnrollmentRuleTrigger
 * Created By      : Deloitte               
 * Created On      : 11/8/2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID      Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Deloitte                11/8/2016                             Initial version
   
*/
public with Sharing class AV_EnrollmentRuleTriggerHelper{
    
    public static void preventDuplicate(List<AV_Rule_for_Enrollment_Assignment__c> Enrule){
    /********************************************************************************************************
    * @author        Deloitte
    * @date          11/8/2016 
    * @description   Allows only one rule record to be created for an enrollment type.
    * @Paramters     List: List containing the rule for enrollment assigment.
    * @return        none
    *********************************************************************************************************/
    
   
    map<ID,AV_Rule_for_Enrollment_Assignment__c > mapAREA= AV_SOQLQueryUtility_Rule_For_Enrollment.getRuleForEnrollment();
    for(AV_Rule_for_Enrollment_Assignment__c RuleVar :Enrule){
    system.debug('key'+RuleVar.AV_Enrollment_Type__c);
        if(mapAREA.containskey(RuleVar.AV_Enrollment_Type__c )){
                RuleVar.addError(Label.AV_Allow_Only_One_RuleForEnrollmentAssignment);
    }
    }
    }
}